#!/bin/bash

# Finalise one run directory manually
# Command: sh finalise_v1.sh <rundir>
# Version 1 (2022-03-10)

RUNDIR=$1

echo "> Finalising $RUNDIR..."
HIST_CONTAINER=$(rucio list-dids user.bpascual.$RUNDIR* | grep -E .*_hist.*\.CONTAINER | sed -e "s/|[ ]*user\.[[:alnum:]]*\:\(.*_hist\) .*DIDType.CONTAINER.*/\1/" | head -n1)
echo $HIST_CONTAINER
if [[ -f $RUNDIR/histograms.root ]]
then
	echo "SKIPPING $RUNDIR: Delete histograms.root if you want to finalise again."
	continue
fi

if [[ $HIST_CONTAINER == "" ]]
then
	echo "SKIPPING $RUNDIR: grid output container not found ($HIST_CONTAINER)"
	continue
fi

echo "Found grid output container: $HIST_CONTAINER"
read -p "Download it? (y to continue) " DOWNLOAD < /dev/tty

if [[ $DOWNLOAD == "y" ]]
then
    echo "> Downloading $HIST_CONTAINER"
    echo "[commmand: rucio download $HIST_CONTAINER]"
    rucio download $HIST_CONTAINER

    if [[ -d $HIST_CONTAINER && ! -z "$(ls -A $HIST_CONTAINER)" ]]
    then
    	HIST_FILE=hist-$(ls $RUNDIR/event_framework_run/hist)
    	echo "> Hadding histograms"
    	echo "[command: hadd $HIST_FILE $HIST_CONTAINER/* ]"
    	hadd $HIST_FILE $HIST_CONTAINER/*
    fi

    if [[ -f $HIST_FILE ]]
    then
    	echo "> Moving histograms to run directory"
    	echo "[command: mv $HIST_FILE $RUNDIR/event_framework_run/$HIST_FILE && ln -s event_framework_run/$HIST_FILE $RUNDIR/histograms.root ]"
    	mv $HIST_FILE $RUNDIR/event_framework_run/$HIST_FILE && ln -s event_framework_run/$HIST_FILE $RUNDIR/histograms.root
    fi

    if [[ -d $HIST_CONTAINER ]]
    then
        echo "Delete downloaded data? [command: rm -r ./$HIST_CONTAINER ]"
        read -p "(y to delete) " DELETE < /dev/tty

        if [[ $DELETE == "y" ]]
        then
            echo "> Deleting downloaded data"
            echo "[command: rm -r ./$HIST_CONTAINER]"
            rm -r ./$HIST_CONTAINER
        fi
    fi
fi

echo "> DONE!"
