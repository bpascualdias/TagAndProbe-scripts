## Version 3 (2022-03-11)
## This script plots the output of the tp efficiency command,
## as well as performs comparisons between two efficiency outputs.

## Command to plot efficiency of one single rundir:
##     python plot_efficiency.py -r <rundir> -l <label> -o <output_directory> -p
## Command to compare models if they have the same name in both rundirs:
##     python plot_efficiency.py -r <rundir1> <rundir2> -l <label1> <label2> -o <output_directory> -c -p
## Command to compare models if they do not have the same name in both rundirs:
##     python plot_efficiency.py -r <rundir1> <rundir2> -l <label1> <label2> -o <output_directory -m '{"<name in rundir1>" : "<name in rundir2>"}' -c -p


from os import makedirs, listdir
import argparse
import ROOT
import pandas as pd
import numpy as np
import seaborn as sns
from copy import deepcopy
import subprocess
import itertools as it
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
#plt.rcParams['font.sans-serif'] = 'Helvetica'


class LaTeX(dict):
    __missing__ = lambda self, key: key
latex = LaTeX({ 'pt' : 'p_{{T}}',
                'eta' : '\eta',
                'primary_cluster_be2_eta' : '\eta',
               })

def plot_2D_from_pandas(df, title, output, values='eff', label='Efficiency'):

    ## Get observables and their binnings
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df['bin_' + obs + '_lo'].values,
                              df['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    x_unit = ' [GeV]' if x_label == 'pt' else ''
    y_unit = ' [GeV]' if y_label == 'pt' else ''

    ## Set matplotlib global parameters
    plt.rc('font', size=10)

    ## Create a 2D plot from the pandas dataframe
    fig, ax = plt.subplots(figsize=(8, 6), tight_layout=True)
    heatmap_data = pd.pivot_table(df, values=values, index=[f'bin_{y_label}_lo'], columns=f'bin_{x_label}_lo')
    xticklabels = [f'{i:.0f}'.format(i) for i in heatmap_data.columns.to_numpy()]
    yticklabels = [f'{i:.2f}'.format(i) for i in heatmap_data.index.to_numpy()]
    ax = sns.heatmap(heatmap_data, annot=True, cmap='Spectral',
                     xticklabels=xticklabels, yticklabels=yticklabels,
                     vmax=df[values].max(), fmt='.2',
                     cbar_kws={'label': label})
    ax.invert_yaxis()
    plt.xticks(np.arange(len(x_binning)), [f'{i:.0f}'.format(i) for i in x_binning], rotation='horizontal')
    plt.yticks(np.arange(len(y_binning)), [f'{i:.2f}'.format(i) for i in y_binning], rotation='horizontal')
    ax.set_xlabel(f'${latex[x_label]}${x_unit}')
    ax.set_ylabel(f'${latex[y_label]}${y_unit}')
    ax.set_title(title)

    ## Save the plot
    fig.savefig(output, dpi=300)
    print(f'> Saved {output}')
    plt.close(fig)

def plot_1D_from_pandas(dfs, output_dir, values, label, name='', title='', ratio=False):

    ## Get observables and their binnings
    df_ref = dfs[list(dfs.keys())[0]]
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df_ref.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df_ref['bin_' + obs + '_lo'].values,
                              df_ref['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    ## Create 1D plots for fixed x bin
    for x_bin in range(1, len(x_binning)):

        x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
        x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

        if x_bin_str not in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']: continue


        ## Gather data points
        curves = {}
        for model, df in dfs.items():

            p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            #yerr = p['eff_errup'].to_numpy()
            bin_edges = np.unique(p[f'bin_{y_label}_lo'].to_list() + p[f'bin_{y_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{y_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves[model] = c

        ## Define plot options
        options = { 'fig_title' : title,
                    'text' : f'{name}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$',
                    'xlabel' : f'${latex[y_label]}$',
                    'ylabel' : label,
                    'ylim' : (0.6, 1.1),
                    'ratio_ylim' : (0.9, 1.1),
                }
        options['text'] = options['text'] + ' [GeV]' if x_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if y_label == 'pt' else options['xlabel']

        if ratio:
            plot_ratio_from_points(curves,
                                   output_name=f'{output_dir}/{values}_ratio_{name}_{x_bin_str}.png',
                                   options=options)
        else:
            for model, c in curves.items():
                plot_from_points({model : c},
                                 output_name=f'{output_dir}/{values}_{model}_{x_bin_str}.png',
                                 options=options)

    ## Create 1D plots for fixed y bin
    for y_bin in range(1, len(y_binning)):

        y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
        y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

        if y_bin_str not in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue

        ## Gather data points
        curves = {}
        for model, df in dfs.items():
            p = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            bin_edges = np.unique(p[f'bin_{x_label}_lo'].to_list() + p[f'bin_{x_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{x_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves[model] = c

        ## Define plot options
        options = { 'fig_title' : title,
                    'text' : f'{name}\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                    'xlabel' : f'${latex[x_label]}$',
                    'ylabel' : label,
                    'ylim' : (0.6, 1.1),
                    'ratio_ylim' : (0.9, 1.1),
                }
        options['text'] = options['text'] + ' [GeV]' if x_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if y_label == 'pt' else options['xlabel']

        if ratio:
            plot_ratio_from_points(curves,
                                   output_name=f'{output_dir}/{values}_ratio_{name}_{y_bin_str}.png',
                                   options=options)
        else:
            for model, c in curves.items():
                plot_from_points({model : c},
                                 output_name=f'{output_dir}/{values}_{model}_{y_bin_str}.png',
                                 options=options)

    ## Create a montage with the projections (using imagemagick)
    shared_name = f'{values}_ratio_{name}' if ratio else f'{values}_{model}'
    bash_command = "montage"
    for x_bin_str in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']:
        bash_command += f' {output_dir}/{shared_name}_{x_bin_str}.png'
    for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']:
        bash_command += f' {output_dir}/{shared_name}_{y_bin_str}.png'
    montage_output = f'{output_dir}/combined_{shared_name}.png'
    bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
    process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
    out, error = process.communicate()
    print(f'> Saved {montage_output}')

def plot_ratio_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 8),
                           gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})
    ## Plot first curve points
    label1 = list(curves.keys())[0]
    x1, y1, w1, yerr1 = curves[label1]
    ax[0].errorbar(x1, y1, xerr=w1/2, yerr=yerr1, label=label1, marker='o', linestyle='none', color='black')

    ## Plot second curve points
    label2 = list(curves.keys())[1]
    x2, y2, w2, yerr2 = curves[label2]
    ax[0].errorbar(x2, y2, xerr=w2/2, yerr=yerr2, label=label2, marker='o', linestyle='none', color='tab:red')

    ## Create legend box for the colors
    ax[0].legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ax[0].annotate(options['text'], xy=(0.05, 0.85), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax[0].semilogy()
    ylim = options['ylim'] if 'ylim' in options.keys() else (0.55, 1.1)
    ax[0].set_ylim(ylim)
    ax[0].set_ylabel(options['ylabel'], loc='top')
    ax[0].yaxis.grid(True, alpha=0.5, which='major')
    ax[0].xaxis.set_minor_locator(AutoMinorLocator())
    ax[0].yaxis.set_minor_locator(AutoMinorLocator())
    ax[0].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[0].tick_params(which='major', axis='both', length=12)
    ax[0].tick_params(which='minor', axis='both', length=8)

    ## Plot ratio
    r = y1/y2
    ax[1].errorbar(x2, r, xerr=w2/2, marker='o', linestyle='none', color='black')
    #ax[1].hlines(1.0, min(x2), max(x2), color='black')
    ratio_ylim = options['ratio_ylim'] if 'ratio_ylim' in options.keys() else (0.9, 1.1)
    ax[1].set_ylim(ratio_ylim)
    ax[1].set_ylabel(f'{label1}/{label2}', loc='center')
    ax[1].set_xlabel(options['xlabel'], loc='right')
    ax[1].yaxis.grid(True, alpha=0.5, which='both')
    ax[1].yaxis.set_minor_locator(AutoMinorLocator())
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[1].tick_params(which='major', axis='both', length=12)
    ax[1].tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

def plot_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, figsize=(8, 8))#,
                           #gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})

    ## Plot curve points
    for i, label in enumerate(curves.keys()):
        x, y, w, yerr = curves[label]
        color = options['colors'][i] if 'colors' in options else 'black'
        marker = options['markers'][i] if 'markers' in options else 'o'
        linestyle = options['linestyles'][i] if 'linestyles' in options else 'none'
        ax.errorbar(x, y, xerr=w/2, yerr=yerr, label=label, marker=marker, linestyle=linestyle, color=color)

    ## Create legend box for the colors
    ax.legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ax.annotate(options['text'], xy=(0.05, 0.85), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'ylim' in options.keys():
        ax.set_ylim(options['ylim'])
    if 'logy' in options and options['logy']:
        ax.semilogy()
    if 'xlim' in options.keys():
        ax.set_xlim(options['xlim'])
    #ax.set_ylim(top=1e10)
    if 'fig_title' in options:
        ax.set_title(options['fig_title'])
    ax.set_xlabel(options['xlabel'], loc='right')
    ax.set_ylabel(options['ylabel'], loc='top')
    ax.yaxis.grid(True, alpha=0.5, which='major')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax.tick_params(which='major', axis='both', length=12)
    ax.tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

def root_to_pandas(hist, root_file=None, values='values', x='x', y='y', z='z'):

    # Get histogram
    if root_file is None:
        h = hist
    else:
        tfile = ROOT.TFile.Open(root_file ,"READ")
        h = tfile.Get(hist)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
        h.SetDirectory(0)
        tfile.Close()

    # Get histogram data
    data = []
    if isinstance(h, ROOT.TH2):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin) / 1000
            x_up = h.GetXaxis().GetBinUpEdge(x_bin) / 1000
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            eff = h.GetBinContent(x_bin, y_bin)
            err = h.GetBinError(x_bin, y_bin)
            data.append((eff, err, x_dn, x_up, y_dn, y_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi']

    if isinstance(h, ROOT.TH3):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())
        z_binedges = np.array(h.GetZaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin)
            x_up = h.GetXaxis().GetBinUpEdge(x_bin)
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            if x_dn > 1000.0:
                x_dn = x_dn/1000.0
                x_up = x_up/1000.0

            for z_bin in range(1, h.GetNbinsZ()+1):

                z_dn = h.GetZaxis().GetBinLowEdge(z_bin)
                z_up = h.GetZaxis().GetBinUpEdge(z_bin)

                mll = h.GetBinContent(x_bin, y_bin, z_bin)
                mll_err = h.GetBinError(x_bin, y_bin, z_bin)
                data.append((mll, mll_err, x_dn, x_up, y_dn, y_up, z_dn, z_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi', f'bin_{z}_lo', f'bin_{z}_hi']

    # Create pandas dataframe with the histogram data
    data = np.array(data)
    df = pd.DataFrame(data, columns=columns)
    return df


def main():
    ## Define parser arguments and load them
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-o', '--output', help='Name of the output directory where the plots will be saved.', nargs=1, dest='output', default="output")
    parser.add_argument('-r', '--rundirs', help='List of run directories obtained as teh output of tp efficiency', nargs='+', dest='rundirs', default=[])
    parser.add_argument('-l', '--labels', help='Labels to identify the run directories. This will be used, for instance, in the legend of the plots.', nargs='+', dest='labels', default=[])
    parser.add_argument('-m', '--models', help='Mapping of models names between the two rundirs, where the keys are the models in the first rundir and the values are the models in the second rundir.', nargs=1, dest='models', default={})
    parser.add_argument('-p', '--projections', help='If passed, plot the projections for certain bins.', dest='projections', action='store_true', default=False)
    parser.add_argument('-c', '--compare', help='If passed, plot the ratio of the two run directories passed.', dest='compare', action='store_true', default=False)
    parser.add_argument('-g', '--mev_to_gev', help='If passed, convert pt entries from MeV to GeV.', dest='mev_to_gev', action='store_true', default=False)
    parser.add_argument('--pt_lim', help='Set minimum and maximum pt in the form of a tuple.', nargs=1, dest='pt_lim', type=str, default=['None, None'])
    args = parser.parse_args()

    ## Create output directory
    output = args.output[0].replace('/', '') if len(args.output) else 'output'
    makedirs(output, exist_ok=True)

    ## Sanity checks in the arguments
    if args.compare and len(args.rundirs) != 2:
        raise Exception('Please provide only two run directories when performing a comparison.')
    if len(args.rundirs) != len(args.labels):
        raise Exception('Please provide a list of labels that matches the rundirs in order.')

    ## Evaluate arguments
    models = eval(args.models[0]) if len(args.models) != 0 else {}
    pt_lim = eval(args.pt_lim[0])

    ## Load dataframes
    print('> Loading dataframes...')
    dfs = {}
    for label, rundir in zip(args.labels, args.rundirs):
        dfs[label] = {}
        if 'tp-efficiency' in rundir:
            ## New framework efficiency output
            files = [f for f in listdir(rundir) if '_central_value' in f]
            for pkl_file in files:

                ## Get pandas dataframe from pkl file
                df = pd.read_pickle(f'{rundir}/{pkl_file}')
                model = pkl_file.replace('efficiency_', '').replace('_central_value.pkl', '')

                ## Use the same model name of the first rundir
                if len(args.labels) != 1 and label == args.labels[1] and model in models.values():
                    model = list(models.keys())[list(models.values()).index(model)]

                ## Convert MeV to GeV, if applicable
                if args.mev_to_gev:
                    df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                    df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Apply limits on th pt binnings
                if pt_lim[0] != None:
                    df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
                if pt_lim[1] != None:
                    df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

                ## Store the dataframe in a dictionary
                dfs[label][model] = df
        else:
            ## Old framework efficiency output
            root_file = f'{rundir}/output/ScaleFactors.root'
            tfile = ROOT.TFile.Open(root_file ,"READ")
            dirs = [key.GetName() for key in tfile.GetListOfKeys()]
            for model_name, dtype in it.product(dirs, ['Data', 'MC']):
                ## Load efficiency histograms
                hist_name = f'{model_name}/Eff{dtype}_CentralValue_{model_name}'
                h_eff = tfile.Get(hist_name)
                if not h_eff: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
                h_eff.SetDirectory(0)

                hist_name = f'{model_name}/Eff{dtype}_TotalError_{model_name}'
                h_err = tfile.Get(hist_name)
                if not h_err: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
                h_err.SetDirectory(0)

                ## Transform ROOT histogram into pandas dataframe
                ## NOTE: make sure to adapt the observables accordingly
                x_label = 'pt'
                y_label = 'primary_cluster_be2_eta'
                df_eff = root_to_pandas(h_eff, values='eff', x=x_label, y=y_label)
                df_errup = root_to_pandas(h_err, values='eff_errup', x=x_label, y=y_label)
                df_errup['eff_errup'] = df_errup['eff_errup']/2
                df_errdown = root_to_pandas(h_err, values='eff_errdown', x=x_label, y=y_label)
                df_errdown['eff_errdown'] = df_errdown['eff_errdown']/2
                df = pd.merge(df_eff, df_errup)
                df = pd.merge(df, df_errdown)

                # ## Convert MeV to GeV, if applicable
                # if args.mev_to_gev:
                #     df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                #     df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Use the same model name of the first rundir
                if label == args.labels[1] and model_name in models.values():
                    model_name = list(models.keys())[list(models.values()).index(model_name)]

                if pt_lim[0] != None:
                    df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
                if pt_lim[1] != None:
                    df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

                model = f'{dtype}_{model_name.replace("_d0z0_v13", "")}'
                dfs[label][model] = df

            tfile.Close()

    ## Plot efficiency for all models
    if not args.compare:
        for label in args.labels:
            output_dir = f'{output}/{label}' if len(args.labels) > 1 else output
            makedirs(output_dir, exist_ok=True)
            for model, df in dfs[label].items():

                ## Plot 2D efficiency
                output_path = f'{output_dir}/eff_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff', label='Efficiency',
                                    title=f'Efficiency: {model.replace("_", " ")}')

                ## Plot 2D total error
                output_path = f'{output_dir}/error_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff_errup', label='Efficiency total error',
                                    title=f'Efficiency total error: {model.replace("_", " ")}')

                ## Plot 1D projections
                if args.projections:
                    projections_dir = f'{output_dir}/projections'
                    makedirs(projections_dir, exist_ok=True)

                    plot_1D_from_pandas({model : df}, output_dir=projections_dir,
                                        values='eff', label='Efficiency')

    ## Plot ratio for all models
    if args.compare:
       output_dir = f'{output}/comparison' if args.projections else output
       makedirs(output_dir, exist_ok=True)

       shared_models = [i for i in dfs[args.labels[0]] if i in dfs[args.labels[1]]]
       print(f'Shared models to be compared: {shared_models}')

       for model in shared_models:

           d = {label : dfs[label][model] for label in args.labels}
           label1, df1 = list(d.items())[0]
           label2, df2 = list(d.items())[1]
           # print(df1['bin_pt_lo'].to_numpy()[0], df1['bin_pt_hi'].to_numpy()[-1])
           # print(df2['bin_pt_lo'].to_numpy()[0], df2['bin_pt_hi'].to_numpy()[-1])
           if df1.shape[0] != df2.shape[0]:
               raise Exception(f'Dataframes do not have the same binning: {df1.shape} vs {df2.shape}\n{np.unique(df1["bin_pt_lo"].to_numpy())}\n{np.unique(df2["bin_pt_lo"].to_numpy())}')

           df_ratio = deepcopy(df1)
           df_ratio['eff'] = df1['eff']/df2['eff'] - np.ones(df1['eff'].shape[0])


           ## Plot 2D ratio
           output_path = f'{output_dir}/ratio_{model}.png'
           plot_2D_from_pandas(df_ratio, output=output_path,
                               values='eff', label='Efficiency',
                               title=f'Efficiency relative error ({label1}/{label2} - 1): {model}')

           ## Plot 1D ratio projections
           if args.projections:
               projections_dir = f'{output_dir}/projections'
               makedirs(projections_dir, exist_ok=True)

               plot_1D_from_pandas(d, output_dir=projections_dir,
                                   values='eff', label='Efficiency',
                                   name=model,
                                   ratio=args.compare)

if __name__ == '__main__':
    main()
