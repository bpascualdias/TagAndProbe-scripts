## Version 1 (2022-03-10)
## This script plots the invariant mass distirbutions from the
## output of the tp efficiency command.
##
## Efficiency commands:
##    Single rundir:
##        python tp-plot.py --eff -r <rundir> -l <label> -o <output_directory>
##    Comparison:
##        python tp-plot.py --eff -r <rundir1> <rundir2> -l <label1> <label2> -o <output_directory> -c
##
## Invariant mass commands:
##    Single rundir:
##       python tp-plot.py --mll -r <rundir> -l <label> -o <output_directory> -b <mc_rundir>
##    Comparison:
##       python tp-plot.py --mll -r <rundir1> <rundir2> -l <label1> <label2> -o <output_directory> -b <mc_rundir> -c

from os import makedirs, listdir, readlink
import argparse
import ROOT
import uproot
import pandas as pd
import numpy as np
import seaborn as sns
import itertools as it
from copy import deepcopy
from tabulate import tabulate
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import heppy as hp

class LaTeX(dict):
    __missing__ = lambda self, key: key
latex = LaTeX({ 'pt' : 'p_{{T}}',
                'eta' : '\eta',
                'primary_cluster_be2_eta' : '\eta',
                'm' : 'm_{{ee}}'
               })

## Load invariant mass historams as dataframes
def load_selection(args, rundirs):
    dfs = {}
    for label, rundir in rundirs.items():
        dfs[label] = {}


        new_fw = True if 'tp-efficiency' in rundir or 'tp-selection' in rundir or len([f for f in listdir(rundir) if 'tp-selection' in f]) != 0 else False

        if new_fw and 'tp-selection' in rundir:
            ## New framework selection output
            print('> Reading output from tp analyse')
            if 'tp-selection' in rundir and 'data' in rundir:
                data_root_file = f'{rundir}/{readlink(f"{rundir}/histograms.root")}'
                mc_root_file = None
            elif 'tp-selection' in rundir and 'data' not in rundir:
                mc_root_file = f'{rundir}/{readlink(f"{rundir}/histograms.root")}'
                data_root_file = None
            else:
                data_dir = [f for f in listdir(rundir) if 'tp-selection' in f and 'data' in f ][0]
                data_root_file = f'{rundir}/{data_dir}/{readlink(f"{rundir}/{data_dir}/histograms.root")}'
                mc_dir = [f for f in listdir(rundir) if 'tp-selection' in f and 'data' not in f ][0]
                mc_root_file = f'{rundir}/{mc_dir}/{readlink(f"{rundir}/{mc_dir}/histograms.root")}'

        elif new_fw and 'tp-efficiency' in rundir:
            ## New framework efficiency output
            print('> Reading output from tp efficiency')
            models = [i.replace('histograms_','').replace('.pkl', '') for i in listdir(rundir) if 'histograms' in i]

            for model in models:
                df = pd.read_pickle(f'{rundir}/histograms_{model}.pkl')

                df = df[ (df['bin_m_lo'] >= 60) & (df['bin_m_hi'] <= 250) ]
                df = df[ (df['bin_pt_lo'] >= 15) ]

                # dfs[label][model] = df

                ## Convert MeV to GeV, if applicable
                if args.mev_to_gev:
                    df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                    df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Store the dataframe in a dictionary
                dfs[label][model] = df

                mc_root_file = None
                data_root_file = None
        else:
            ## Old framework selection output
            print('> Reading output from TagAndProbeFrame')
            mc_root_file = f'{rundir}/merged-hist-mc.root'
            data_root_file = f'{rundir}/merged-hist-data.root'

        # print(f'    MC= {mc_root_file}\n    Data={data_root_file}')
        root_files = {}
        if mc_root_file is not None:
            root_files['MC'] = mc_root_file
        if data_root_file is not None:
            root_files['Data'] = data_root_file

        for dtype, root_file in root_files.items():
            with uproot.open(root_file) as urfile:

                hist_paths = {}
                ## Cutflow plots
                if args.cutflow:
                    keys = [k.split(';')[0] for k in urfile.keys() if 'cutflow' in k.lower()]

                    for key in keys:

                        uid = f'{dtype}_{key.split("_")[0]}' if 'h_' not in key else f'{dtype}_{key.replace("h_", "").split("_")[0]}'

                        # print(label, uid)
                        # df = hp.readroot(root_file, key)
                        # df = root_to_pandas(key, root_file=root_file, values='entries', x='cuts')
                        h = urfile[key]
                        # tfile = ROOT.TFile.Open(root_file ,"READ")
                        # h = tfile.Get(key)
                        # if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (key, root_file))
                        # h.SetDirectory(0)
                        # tfile.Close()

                        dfs[label][uid] = h

                ## Invariant mass plots
                else:
                    keys = get_histogram_keys(urfile)

                    for key in keys:
                        if key.observables_string not in ['m', 'charge', 'pt-primary_cluster_be2_eta-m']:
                            continue
                        if key.objects_string not in ['tag_TightLLH', 'tag_denominator', 'tag_background', 'tag_pass_track_quality'
                                                    'tag_VeryLooseLLH', 'tag_FailVeryLooseLLH', 'tag_background_d0z0',
                                                      'TightLLH-TightLLH-tag_TightLLH', 'denominator-denominator-tag_denominator', 'pass_track_quality-pass_track_quality-tag_pass_track_quality', 'background-background-tag_background']:
                            continue
                        # if key.tp_pair not in ['base_selection']:
                        #     continue

                        uid = f'{dtype}_{key.tp_pair}_{key.objects_string}_{key.observables_string}'
                        if len(key.observables) == 1:
                            # df = hp.readroot(root_file, key.path())
                            x = key.observables[0]
                            y, z = None, None
                            df = root_to_pandas(key.path(), root_file=root_file, values='entries', x=x, y=y, z=z)
                        if len(key.observables) == 3:
                            x, y, z = key.observables
                            df = root_to_pandas(key.path(), root_file=root_file, values='entries', x=x, y=y, z=z)

                        dfs[label][uid] = df

                        ## Copy "Denominator" histogram as pass-track-quality for comparison
                        if not new_fw and 'denominator-denominator-tag_denominator_pt-primary_cluster_be2_eta-m' in uid:
                            copy_uid = uid.replace('denominator-denominator-tag_denominator_pt-primary_cluster_be2_eta-m', 'pass_track_quality-pass_track_quality-tag_pass_track_quality_pt-primary_cluster_be2_eta-m')
                            dfs[label][copy_uid] = df

    return dfs

## Load efficiency results as dataframes
def load_efficiency(args):
    dfs = {}
    pt_lim = eval(args.pt_lim[0])
    for label, rundir in zip(args.labels, args.rundirs):
        dfs[label] = {}
        if 'tp-efficiency' in rundir:
            ## New framework efficiency output
            files = [f for f in listdir(rundir) if '_central_value' in f]
            for pkl_file in files:

                ## Get pandas dataframe from pkl file
                df = pd.read_pickle(f'{rundir}/{pkl_file}')
                model = pkl_file.replace('efficiency_', '').replace('_central_value.pkl', '')

                ## Use the same model name of the first rundir
                if len(args.labels) != 1 and label == args.labels[1] and model in models.values():
                    model = list(models.keys())[list(models.values()).index(model)]

                ## Convert MeV to GeV, if applicable
                if args.mev_to_gev:
                    df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                    df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Apply limits on th pt binnings
                if pt_lim[0] != None:
                    df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
                if pt_lim[1] != None:
                    df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

                ## Store the dataframe in a dictionary
                dfs[label][model] = df

        else:
            ## Old framework efficiency output
            root_file = f'{rundir}/output/ScaleFactors.root'
            tfile = ROOT.TFile.Open(root_file ,"READ")
            dirs = [key.GetName() for key in tfile.GetListOfKeys()]
            for model_name, dtype in it.product(dirs, ['Data', 'MC']):
                ## Load efficiency histograms
                hist_name = f'{model_name}/Eff{dtype}_CentralValue_{model_name}'
                h_eff = tfile.Get(hist_name)
                if not h_eff: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
                h_eff.SetDirectory(0)

                hist_name = f'{model_name}/Eff{dtype}_TotalError_{model_name}'
                h_err = tfile.Get(hist_name)
                if not h_err: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
                h_err.SetDirectory(0)

                ## Transform ROOT histogram into pandas dataframe
                ## NOTE: make sure to adapt the observables accordingly
                x_label = 'pt'
                y_label = 'primary_cluster_be2_eta'
                df_eff = root_to_pandas(h_eff, values='eff', x=x_label, y=y_label)
                df_errup = root_to_pandas(h_err, values='eff_errup', x=x_label, y=y_label)
                df_errup['eff_errup'] = df_errup['eff_errup']/2
                df_errdown = root_to_pandas(h_err, values='eff_errdown', x=x_label, y=y_label)
                df_errdown['eff_errdown'] = df_errdown['eff_errdown']/2
                df = pd.merge(df_eff, df_errup)
                df = pd.merge(df, df_errdown)

                # ## Convert MeV to GeV, if applicable
                # if args.mev_to_gev:
                #     df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                #     df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Use the same model name of the first rundir
                # if label == args.labels[1] and model_name in models.values():
                    # model_name = list(models.keys())[list(models.values()).index(model_name)]

                if pt_lim[0] != None:
                    df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
                if pt_lim[1] != None:
                    df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

                model = f'{dtype}_{model_name.replace("_d0z0_v13", "")}'
                dfs[label][model] = df

            tfile.Close()

    return dfs

## Returns the number of events in the sample (from CutBookkeeper_XYZXYZ_1_NOSYS)
def get_N_sample(rundir, fw='new', weights=False):
    if fw == 'new':
        root_file = f'{rundir}/{readlink(f"{rundir}/histograms.root")}'
        tfile = ROOT.TFile.Open(root_file ,"READ")
        hist = [key.GetName() for key in tfile.GetListOfKeys() if 'CutBookkeeper' in key.GetName()][0]
        h = tfile.Get(hist)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
        h.SetDirectory(0)
        tfile.Close()

        N_histogram = h.GetBinContent(1) # take contents of first bin (initial events)
        N_sample = h.GetBinContent(2) # take contents of second bin (initial sum of weights = n_events * cross-section)
        N_sum = h.GetBinContent(3) # take contents of third bin

        out = N_sample if weights else N_histogram

        # print(f'1 = {N_histogram}, 2 = {N_sample}, 3 = {N_sum}')
        # print(f'2/1 = {N_sample/N_histogram}, 3/2 = {N_sum/N_sample}')
        # print(f'N_sample = {N_sample}, N_histogram = {N_histogram}, ratio = {N_sample/N_histogram}')

        # N_sample = N_sample/2
        # N_events =  512995200 # raw number of events in the full sample (from ami)
        # N_sample = N_sample/N_histogram * N_events
        # print(f'ADJUSTED N_sample new = {N_sample}')


    if fw == 'old':
        root_file = f'{rundir}/merged-hist-mc.root'
        tfile = ROOT.TFile.Open(root_file ,"READ")
        hist = [key.GetName() for key in tfile.GetListOfKeys() if 'nEvents' in key.GetName()][0]
        h = tfile.Get(hist)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
        h.SetDirectory(0)
        tfile.Close()

        N_sample = h.GetBinContent(1) # take contents of first bin

        out = N_sample

        # print('N_sample', 'old', out)

        # N_events =  512995200 # raw number of events in the full sample (from ami)
        # N_sample = 1 * N_events
        # print(f'ADJUSTED N_sample old = {N_sample}')

        # N_sample = 2*N_sample

    return out

## Load ROOT histogram and transform it to a pandas dataframe
def root_to_pandas(hist, root_file=None, values='values', x='x', y='y', z='z'):

    # Get histogram
    if root_file is None:
        h = hist
    else:
        tfile = ROOT.TFile.Open(root_file ,"READ")
        h = tfile.Get(hist)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
        h.SetDirectory(0)
        tfile.Close()

    # Get histogram data
    data = []

    if isinstance(h, ROOT.TH3):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())
        z_binedges = np.array(h.GetZaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin)
            x_up = h.GetXaxis().GetBinUpEdge(x_bin)
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            if x_dn > 1000.0:
                x_dn = x_dn/1000.0
                x_up = x_up/1000.0

            for z_bin in range(1, h.GetNbinsZ()+1):

                z_dn = h.GetZaxis().GetBinLowEdge(z_bin)
                z_up = h.GetZaxis().GetBinUpEdge(z_bin)

                mll = h.GetBinContent(x_bin, y_bin, z_bin)
                mll_err = h.GetBinError(x_bin, y_bin, z_bin)
                data.append((mll, mll_err, x_dn, x_up, y_dn, y_up, z_dn, z_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi', f'bin_{z}_lo', f'bin_{z}_hi']

    elif isinstance(h, ROOT.TH2):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin)
            x_up = h.GetXaxis().GetBinUpEdge(x_bin)
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            if x_dn > 1000.0:
                x_dn = x_dn/1000.0
                x_up = x_up/1000.0

            eff = h.GetBinContent(x_bin, y_bin)
            err = h.GetBinError(x_bin, y_bin)
            data.append((eff, err, x_dn, x_up, y_dn, y_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi']

    elif isinstance(h, ROOT.TH1):
        x_binedges = np.array(h.GetXaxis().GetXbins())

        for x_bin in range(1, h.GetNbinsX()+1): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin)
            x_up = h.GetXaxis().GetBinUpEdge(x_bin)

            obs = h.GetBinContent(x_bin)
            err = h.GetBinError(x_bin)
            data.append((obs, err, x_dn, x_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi']

    # Create pandas dataframe with the histogram data
    data = np.array(data)
    df = pd.DataFrame(data, columns=columns)
    return df

## Get points from pandas:
def pandas_to_points(df, value, obs_label,
                     x_label='', x_bin=(None,None),
                     y_label='', y_bin=(None,None),
                     binning=None):

    if x_label == '' and y_label == '':
        p = df
    else:
        entries = [c for c in df.columns if 'bin' not in c]
        x_bin_dn, x_bin_up = x_bin
        y_bin_dn, y_bin_up = y_bin
        x_bin_exists = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)].shape[0]
        y_bin_exists = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)].shape[0]
        if not x_bin_exists:
            p = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]
            p = df.groupby([f'bin_{obs_label}_lo', f'bin_{obs_label}_hi'], as_index=False)[entries].agg('sum')
        elif not y_bin_exists:
            p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)]
            p = df.groupby([f'bin_{obs_label}_lo', f'bin_{obs_label}_hi'], as_index=False)[entries].agg('sum')
        else:
            p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up) &
                (df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]

    y = p[value].to_numpy()
    #yerr = np.array([p[f'{value}_errdown'].abs().to_numpy(), p[f'{value}_eff_errup'].to_numpy()])
    #yerr = p[f'{value}_err'].to_numpy()
    yerr = np.zeros(len(y))
    bin_edges = np.unique(p[f'bin_{obs_label}_lo'].to_list() + p[f'bin_{obs_label}_hi'].to_list())
    bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
    x = p[f'bin_{obs_label}_lo'].to_numpy() + bin_sizes/2
    # x = bin_edges
    c = (x, y, bin_sizes, yerr)

    if binning is not None:
        print(binning)

    return c

## Plot from data points
def plot_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, figsize=(8, 8))#,
                           #gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})

    ## Plot curve points
    for i, label in enumerate(curves.keys()):
        x, y, w, yerr = curves[label]
        color = options['colors'][i] if 'colors' in options else 'black'
        marker = options['markers'][i] if 'markers' in options else 'o'
        linestyle = options['linestyles'][i] if 'linestyles' in options else 'none'

        is_histogram = options['is_histogram'] if 'is_histogram' in options else False
        if is_histogram:
            ax.plot(x, y, label=label, marker=marker, linestyle=linestyle, color=color)
        else:
            ax.errorbar(x, y, xerr=w/2, yerr=yerr, label=label, marker=marker, linestyle=linestyle, color=color)

    ## Create legend box for the colors
    ax.legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ax.annotate(options['text'], xy=(0.05, 0.85), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax.semilogy()
    if 'ylim' in options.keys():
        ax.set_ylim(options['ylim'])
    if 'xlim' in options.keys():
        ax.set_xlim(options['xlim'])
    #ax.set_ylim(top=1e10)
    if 'fig_title' in options:
        ax.set_title(options['fig_title'])
    ax.set_xlabel(options['xlabel'], loc='right')
    ax.set_ylabel(options['ylabel'], loc='top')
    ax.yaxis.grid(True, alpha=0.5, which='major')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax.tick_params(which='major', axis='both', length=12)
    ax.tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

## Normalise MC curves
def normalise_MC(c, label, rundir='', verbose=False):

    #N_data_sample = 103405 # number of events in the data sample the MC will be compared to
    N_data_sample =  512995200# number of events in the data sample the MC will be compared to
    N_full_sample =  512995200 # raw number of events in the full sample (from ami)
    luminosity = 58450.1 # pb^{-1} from https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data18_13TeV/20190318/notes.txt
    cross_section = 1901.1*1*1 # pb (cross-section * filter efficiency * k-factor) from https://ami.in2p3.fr/
    if 'old' in label:
        N_sample = get_N_sample(rundir, fw='old') # number of events in the sample (from h_nEvents)
        # scale_value = 1
        scale_value = 1
        # scale_value = N_data_sample/N_full_sample * luminosity * (1/N_sum_of_weights) * cross_section
    else:
        N_sample = get_N_sample(rundir, fw='new') # number of events in the sample (from CutBookkeeper_XYZXYZ_1_NOSYS)
        N_sum_of_weights = get_N_sample(rundir, fw='new', weights=True) # number of events in the sample (from CutBookkeeper_XYZXYZ_1_NOSYS)
        # print('AAA', N_sample, N_weights)
        scale_value = N_data_sample/N_full_sample * luminosity * (1/N_sum_of_weights) * cross_section
        # scale_value = 1


    # print(label, N_sample, scale_value)

    unweighted_mc_entries = c[1]
    if verbose:
        print(f'Scale value for {label} = {scale_value}')
    weighted_mc_entries = unweighted_mc_entries * scale_value

    updated_mc = list(c)
    updated_mc[1] = weighted_mc_entries
    new_c = tuple(updated_mc)

    return new_c

## Plot ratio from data points
def plot_ratio_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 8),
                           gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})
    ## Plot first curve points
    label1 = list(curves.keys())[0]
    x1, y1, w1, yerr1 = curves[label1]
    ax[0].errorbar(x1, y1, xerr=w1/2, yerr=yerr1, label=label1,
                   marker=options['markers'][0] if 'markers' in options else 'o',
                   linestyle=options['linestyles'][0] if 'linestyles' in options else 'none', capsize=10,
                   color=options['colors'][0] if 'colors' in options else 'black')

    ## Plot second curve points
    label2 = list(curves.keys())[1]
    x2, y2, w2, yerr2 = curves[label2]
    ax[0].errorbar(x2, y2, xerr=w2/2, yerr=yerr2, label=label2,
                   marker=options['markers'][1] if 'markers' in options else 'o',
                   linestyle=options['linestyles'][1] if 'linestyles' in options else 'none', capsize=10,
                   color=options['colors'][1] if 'colors' in options else 'tab:red')

    ## Create legend box for the colors
    ax[0].legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ypos = 0.85 if len(options['text'].split('\n')) < 2 else 0.8
        ax[0].annotate(options['text'], xy=(0.05, ypos), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax[0].semilogy()
    if 'ylim' in options:
        ax[0].set_ylim(options['ylim'])
    if 'fig_title' in options:
        ax[0].set_title(options['fig_title'])
    ax[0].set_ylabel(options['ylabel'], loc='top')
    ax[0].yaxis.grid(True, alpha=0.5, which='major')
    ax[0].xaxis.set_minor_locator(AutoMinorLocator())
    ax[0].yaxis.set_minor_locator(AutoMinorLocator())
    ax[0].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[0].tick_params(which='major', axis='both', length=12)
    ax[0].tick_params(which='minor', axis='both', length=8)

    ## Plot ratio
    r = y1/y2
    ax[1].errorbar(x2, r, xerr=w2/2, marker='o', linestyle='none', color='black')
    #ax[1].hlines(1.0, min(x2), max(x2), color='black')
    if 'ratio_ylim' in options:
        ax[1].set_ylim(options['ratio_ylim'])
    if 'ratio_ylabel' in options:
        ax[1].set_ylabel(options['ratio_ylabel'], loc='center')
    else:
        ax[1].set_ylabel(f'{label1}/{label2}', loc='center')
    ax[1].set_xlabel(options['xlabel'], loc='right')
    ax[1].yaxis.grid(True, alpha=0.5, which='both')
    ax[1].yaxis.set_minor_locator(AutoMinorLocator())
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[1].tick_params(which='major', axis='both', length=12)
    ax[1].tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

## Plot efficiency 2D as heatmap from pandas
def plot_2D_from_pandas(df, title, output, values='eff', label='Efficiency'):

    ## Get observables and their binnings
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df['bin_' + obs + '_lo'].values,
                              df['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    x_unit = ' [GeV]' if x_label == 'pt' else ''
    y_unit = ' [GeV]' if y_label == 'pt' else ''

    ## Set matplotlib global parameters
    plt.rc('font', size=10)

    ## Create a 2D plot from the pandas dataframe
    fig, ax = plt.subplots(figsize=(8, 6), tight_layout=True)
    heatmap_data = pd.pivot_table(df, values=values, index=[f'bin_{y_label}_lo'], columns=f'bin_{x_label}_lo')
    xticklabels = [f'{i:.0f}'.format(i) for i in heatmap_data.columns.to_numpy()]
    yticklabels = [f'{i:.2f}'.format(i) for i in heatmap_data.index.to_numpy()]
    ax = sns.heatmap(heatmap_data, annot=True, cmap='Spectral',
                     xticklabels=xticklabels, yticklabels=yticklabels,
                     vmax=df[values].max(), fmt='.2',
                     cbar_kws={'label': label})
    ax.invert_yaxis()
    plt.xticks(np.arange(len(x_binning)), [f'{i:.0f}'.format(i) for i in x_binning], rotation='horizontal')
    plt.yticks(np.arange(len(y_binning)), [f'{i:.2f}'.format(i) for i in y_binning], rotation='horizontal')
    ax.set_xlabel(f'${latex[x_label]}${x_unit}')
    ax.set_ylabel(f'${latex[y_label]}${y_unit}')
    ax.set_title(title)

    ## Save the plot
    fig.savefig(output, dpi=300)
    print(f'> Saved {output}')
    plt.close(fig)

## Plot efficiency 1D
def plot_1D_from_pandas(dfs, output_dir, values, label, name='', title='', ratio=False):

    ## Get observables and their binnings
    df_ref = dfs[list(dfs.keys())[0]]
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df_ref.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df_ref['bin_' + obs + '_lo'].values,
                              df_ref['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    ## Create 1D plots for fixed x bin
    for x_bin in range(1, len(x_binning)):

        x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
        x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

        if x_bin_str not in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']: continue


        ## Gather data points
        curves = {}
        for model, df in dfs.items():

            p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            #yerr = p['eff_errup'].to_numpy()
            bin_edges = np.unique(p[f'bin_{y_label}_lo'].to_list() + p[f'bin_{y_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{y_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves[model] = c

        ## Define plot options
        options = { 'fig_title' : title,
                    'text' : f'{name}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$',
                    'xlabel' : f'${latex[y_label]}$',
                    'ylabel' : label,
                    'ylim' : (0.6, 1.1),
                    'ratio_ylim' : (0.999, 1.001),
                }
        # if name != '' and name.split('_')[1] == 'LooseAndBLayerLLH':
        #     options['ylim'] = 0.8, 1.1
        # if name != '' and name.split('_')[1] == 'MediumLLH':
        #     options['ylim'] = 0.7, 1.1
        # if name != '' and name.split('_')[1] == 'TightLLH':
        #     options['ylim'] = 0.6, 1.1
        options['text'] = options['text'] + ' [GeV]' if x_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if y_label == 'pt' else options['xlabel']

        if ratio:
            plot_ratio_from_points(curves,
                                   output_name=f'{output_dir}/{values}_ratio_{name}_{x_bin_str}.png',
                                   options=options)
        else:
            for model, c in curves.items():
                plot_from_points({model : c},
                                 output_name=f'{output_dir}/{values}_{model}_{x_bin_str}.png',
                                 options=options)

    ## Create 1D plots for fixed y bin
    for y_bin in range(1, len(y_binning)):

        y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
        y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

        if y_bin_str not in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue

        ## Gather data points
        curves = {}
        for model, df in dfs.items():
            p = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            bin_edges = np.unique(p[f'bin_{x_label}_lo'].to_list() + p[f'bin_{x_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{x_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves[model] = c

        ## Define plot options
        options = { 'fig_title' : title,
                    'text' : f'{name}\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                    'xlabel' : f'${latex[x_label]}$',
                    'ylabel' : label,
                    'ylim' : (0.6, 1.1),
                    'ratio_ylim' : (0.999, 1.001),
                }
        # if name != '' and name.split('_')[1] == 'LooseAndBLayerLLH':
        #     options['ylim'] = 0.8, 1.1
        # if name != '' and name.split('_')[1] == 'MediumLLH':
        #     options['ylim'] = 0.7, 1.1
        # if name != '' and name.split('_')[1] == 'TightLLH':
        #     options['ylim'] = 0.6, 1.1
        options['text'] = options['text'] + ' [GeV]' if x_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if y_label == 'pt' else options['xlabel']

        if ratio:
            plot_ratio_from_points(curves,
                                   output_name=f'{output_dir}/{values}_ratio_{name}_{y_bin_str}.png',
                                   options=options)
        else:
            for model, c in curves.items():
                plot_from_points({model : c},
                                 output_name=f'{output_dir}/{values}_{model}_{y_bin_str}.png',
                                 options=options)

    ## Create a montage with the projections (using imagemagick)
    shared_name = f'{values}_ratio_{name}' if ratio else f'{values}_{model}'
    bash_command = "montage"
    for x_bin_str in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']:
        bash_command += f' {output_dir}/{shared_name}_{x_bin_str}.png'
    for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']:
        bash_command += f' {output_dir}/{shared_name}_{y_bin_str}.png'
    montage_output = f'{output_dir}/combined_{shared_name}.png'
    bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
    process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
    out, error = process.communicate()
    print(f'> Saved {montage_output}')

## From TagAndProbe tp-plot
class HistKey(object):
    def __init__(self, key_str):
        """Keeps information about a histogram key in a ROOT file.

        Arguments:
            key_str (``str``): key (= path of the histogram inside the ROOT
                file) as string. NOTE: must be for a histogram!
        """
        super(HistKey, self).__init__()
        # Remove the trailing semicolon + number that's often present, but not
        # needed
        key_str = key_str.split(';')[0]
        self.key_tokens = key_str.split('/')

        self.old_fw = key_str if (len(self.key_tokens) == 2) else False
        if self.old_fw:
            ## Old framework histogram!
            if not 'Mll_3d' in key_str[-6:]:
                raise ValueError('Nope!')
            if 'Den' in key_str and key_str.split('_')[0] == 'TightLLH':
                tppair, object = {
                    # 'SS' : ('same-sign_selection', 'pass_track_quality'),
                    'SS' : ('same-sign_selection', 'denominator'),
                    # 'DenTempl' : ('background_selection', 'background'),
                    'DenTempl' : ('opposite-sign_selection', 'background'),
                    # 'Den' : ('base_selection', 'denominator')
                    # 'Den' : ('opposite-sign_selection', 'pass_track_quality')
                    'Den' : ('opposite-sign_selection', 'denominator')
                }[key_str[:-7].split('_')[-1]]
            elif 'Num' in key_str and 'NumTempl' not in key_str:
                object = key_str.split('_')[0]
                tppair = {
                    'SS' : 'same-sign_selection',
                    # 'NumTempl' : 'background_selection',
                    # 'Num' : 'base_selection'
                    'Num' : 'opposite-sign_selection'
                }[key_str[:-7].split('_')[-1]]
            else:
                raise ValueError('Nope!')


            self.key_tokens = [tppair, 'nominal', f'{object}-{object}-tag_{object}', 'reco', 'pt-primary_cluster_be2_eta-m']

        elif len(self.key_tokens) != 5:
            raise ValueError('Got an unexpected path for an object inside a '
                'ROOT file: "' + key_str + '". That does not look like it '
                'corresponds to a histogram. Please check!')

    @property
    def tp_pair(self):
        """Returns the tag-and-probe pair's unique identifier."""
        return self.key_tokens[0]

    @property
    def sys_variation(self):
        """Returns the systematic variation name."""
        return self.key_tokens[1]

    @property
    def objects_string(self):
        """Returns the (hyphen-separated) unique identifier(s) of the object(s).
        """
        return self.key_tokens[2]

    @property
    def objects(self):
        """Returns the unique identifier(s) of the object(s) as tuple."""
        return tuple(self.objects_string.split('-'))

    @property
    def input_type(self):
        """Returns the type of input used for the histogram."""
        return self.key_tokens[3]

    @property
    def observables_string(self):
        """Returns the (hyphen-separated) observable(s)."""
        return self.key_tokens[4]

    @property
    def observables(self):
        """Returns the observable(s) as tuple."""
        return tuple(self.observables_string.split('-'))

    @property
    def dimension(self):
        """Returns the dimension of the histogram."""
        return len(self.observables)

    def path(self):
        """Returns the path (key) of the histogram inside the ROOT file."""
        if self.old_fw:
            return self.old_fw
        return '/'.join(self.key_tokens)

def get_histogram_keys(urfile):
    """Returns key strings corresponding to histograms as list of HistKeys.

        Arguments:
            urfile (``uproot.reading.ReadOnlyDirectory``): uproot handle to the
                ROOT file

        Returns:
            ``list`` of ``HistKey``: list of those keys that correspond to
                histograms
    """
    keys = []
    key_strs = urfile.keys()
    for key_str in key_strs:
        if not urfile.classnames()[key_str].startswith('TH'):
            # Not a histogram, ignore!
            continue
        try:
            keys.append(HistKey(key_str))
        except ValueError:
            # That wasn't a valid histogram key, ignore!
            pass
    return keys



def main():
    ## Define parser arguments and load them
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-o', '--output', help='Name of the output directory where the plots will be saved.', nargs=1, dest='output', default="output")
    parser.add_argument('-r', '--rundirs', help='List of run directories obtained as teh output of tp efficiency', nargs='+', dest='rundirs', default=[])
    parser.add_argument('-l', '--labels', help='Labels to identify the run directories. This will be used, for instance, in the legend of the plots.', nargs='+', dest='labels', default=[])
    parser.add_argument('-p', '--projections', help='If passed, plot the projections for certain bins.', dest='projections', action='store_true', default=False)
    parser.add_argument('-c', '--compare', help='If passed, plot the ratio of the two run directories passed.', dest='compare', action='store_true', default=False)
    parser.add_argument('--only-compare', help='If passed, plot only the comparison plots.', dest='only_compare', action='store_true', default=False)
    parser.add_argument('-b', '--mc_new', help='Run directory of the MC analysis in the new framework, containing the CutBookkeper histogram.', nargs=1, dest='mc_rundir', default='')
    parser.add_argument('-g', '--mev_to_gev', help='If passed, convert pt entries from MeV to GeV.', dest='mev_to_gev', action='store_true', default=False)
    parser.add_argument('--pt-lim', help='Set minimum and maximum pt in the form of a tuple.', nargs=1, dest='pt_lim', type=str, default=['15, 250'])
    parser.add_argument('--cutflow', help='If passed, plot only the cutflow plots.', dest='cutflow', action='store_true', default=False)
    parser.add_argument('--mll', help='If passed, plot invariant mass distributions.', dest='mll', action='store_true', default=False)
    parser.add_argument('--eff', help='If passed, plot efficiency values.', dest='eff', action='store_true', default=False)
    args = parser.parse_args()

    ## Create output directory
    output = args.output[0].replace('/', '') if len(args.output) else 'output'
    makedirs(output, exist_ok=True)

    ## Sanity checks in the arguments
    if args.compare and len(args.rundirs) != 2:
        raise Exception('Please provide only two run directories when performing a comparison.')
    if len(args.rundirs) != len(args.labels):
        raise Exception('Please provide a list of labels that matches the rundirs in order.')

    rundirs = {label : rundir for label, rundir in zip(args.labels, args.rundirs)}

    ## Load histograms
    if args.mll:
        print('> Loading invariant mass histograms')
        dfs = load_selection(args, rundirs)
    elif args.eff:
        print('> Loading efficiency values')
        dfs = load_efficiency(args)
    else:
        raise Exception("Please provide either --eff or --mll option.")

    print('> Dataframes:')
    s = ''
    for label in dfs.keys():
        s += f'  {label}:\n'
        for uid in dfs[label].keys():
            s += f'    {uid}\n'
            # s += f'        {dfs[label][uid].columns}\n'
    print(s)
    # return

    ## Print cutflow information
    if args.cutflow:
        for label in args.labels:
            # output_dir = f'{output}/{label}' if len(args.labels) > 1 else output
            # makedirs(output_dir, exist_ok=True)

            for uid, h in dfs[label].items():

                if uid.split('_')[1] not in ['Cutflow', 'denominator']:
                    continue


                cuts = np.array(h.axis().labels())
                if label == 'old':
                    cuts = ['Event', '','GRL', 'Vertex', 'Trigger', 'TwoStdElectrons', '','','Author', 'Fiducial', 'Pt', 'OQ','', 'TagFiresTrigger', 'TruthTag', 'TruthProbe', 'TagTrackIso', '','JetVeto', '','','TagID', 'MassWindow', 'OS', 'NumberOfProbes', 'PassTrackQuality', 'VeryLooseLLH_v13', 'LooseLLH_v13', 'LooseAndBLayerLLH_v13', 'MediumLLH_v13', 'TightLLH_v13', '']
                counts = h.counts()
                print(cuts)
                print(counts)
                table = np.concatenate((np.array([cuts]).T, np.array([counts]).T), axis=1)

                print(f'\nCutflow from {label} for {uid}:')
                print(tabulate(table, headers=['Cuts', 'Counts'], tablefmt='grid'))

        return

    ## Plot invariant mass distirbutions for each rundir
    if args.mll and not args.only_compare:
        output_dir = f'{output}/{label}'# if len(args.labels) > 1 else output
        makedirs(output_dir, exist_ok=True)

        for label in args.labels:
            output_dir = f'{output}/{label}'# if len(args.labels) > 1 else output
            makedirs(output_dir, exist_ok=True)

            ## Get x, y observables and their binnings
            # uid_ref = 'Data_opposite-sign_selection_denominator-denominator-tag_denominator_pt-primary_cluster_be2_eta-m'
            # uid_ref = [i for i in dfs[args.labels[0]].keys() if 'pt-primary_cluster_be2_eta-m' in i][0]
            uid_ref = [i for i in dfs[args.labels[0]].keys()][0]

            df_ref = dfs[args.labels[0]][uid_ref]
            obs_labels = [c.replace('bin_', '').replace('_lo', '')
                            for c in df_ref.columns if 'bin' in c and '_lo' in c]
            obs_binning = [np.union1d(df_ref['bin_' + obs + '_lo'].values,
                                    df_ref['bin_' + obs + '_hi'].values)
                            for obs in obs_labels
                            ]
            observables = {x: (l, b) for x, l, b in zip(['x', 'y', 'z'], obs_labels, obs_binning)}
            x_label, x_binning = observables['x']
            y_label, y_binning = observables['y']
            z_label, z_binning = observables['z']

            ## Create 1D plots for fixed x bin and y bin
            for x_bin, y_bin in it.product(range(1, len(x_binning)), range(1, len(y_binning))):

                x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
                x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

                y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
                y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

                # print(label, x_bin_str, y_bin_str)

                if x_bin_str not in [f'{x_label}_15_250', f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_80_150']: continue
                if y_bin_str not in [f'{y_label}_-2.47_2.47', f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue


                ## Plot DATA and MONTE-CARLO for numerator and denominator
                ## NOTE: Only for tp-efficiency output histograms
                variations = ['tag_moriond_var1-high-pt_mll_75_105']
                ids = ['TightLLH_d0z0']
                htypes = ['den']
                for id, var, htype in it.product(ids, variations, htypes):

                    if 'tp-efficiency' not in rundirs[label]: continue
                    curves = {}
                    ymax = 0
                    for dtype, value in [('Data','N_'+htype), ('Data','B_'+htype), ('MC','N_'+htype)]:

                        uid = f'{dtype}_{id}_{var}'
                        df = dfs[label][uid]
                        obs_label = 'm'

                        c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                        if 'N_' in value: legend = f'Base {dtype}'
                        if 'BT_' in value: legend = f'Background {dtype}'
                        if 'B_' in value: legend = f'Bkg estimation'


                        ## Normalise MC curves for correct cross-section/luminosity
                        if dtype == 'MC':
                            c = normalise_MC(c, label, args.mc_rundir[0])
                        curves[legend] = c

                        print(legend, c[0][:3], c[1][:3])
                        ymax = max(c[1]) if max(c[1]) > ymax else ymax

                    # Add curve with MC + bkg
                    mc_bkg = list(curves['Base MC'])
                    mc_bkg[1] = curves['Base MC'][1] + curves['Bkg estimation'][1]
                    curves['Expected (MC + bkg)'] = tuple(mc_bkg)

                    # # If numerator, add same-sign distribution
                    # if htype == 'num':
                    #     dtype, value = 'Data', 'N_ss_num'
                    #     uid = f'{dtype}_{model}'
                    #     df = dfs[label][uid]
                    #     c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))
                    #     curves['SS'] = c

                    options = { 'fig_title' : f'{label} {htype}',
                                'text' : f'{id}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                                'xlabel' : f'${latex[obs_label]}$' + ' [GeV]',
                                'ylabel' : 'Entries / 1 GeV',
                                'logy' : False if htype == 'den' else True,
                                'colors' : ['black', 'tab:blue', 'tab:orange', 'tab:red', 'tab:green'],
                                'markers' : ['o', 'o', '', '', 'o'],
                                'linestyles' : ['', '', '-', '-', ''],
                                'ylim' : (0, 1.5*ymax) if htype == 'den' else (0, 1.5*10*ymax),
                                # 'xlim' : (60, 150),
                                }
                    plot_from_points(curves,
                                    output_name=f'{output}/mll_{label}_{id}_{var}_{htype}_{x_bin_str}_{y_bin_str}.png',
                                    options=options)





                # ## PLOT: distributions for fixed data type from 3D histogram
                # model = 'TightLLH'
                # obs_label = 'pt-primary_cluster_be2_eta-m'
                # for dtype in ['Data', 'MC']:
                #     print(f'> Plotting {dtype} distributions for bins {x_bin_str} and {y_bin_str}')
                #     curves = {}
                #     ymax = 0
                #     for legend, htype in { 'Denominator' : 'denominator',
                #                            # 'Pass_track_quality' : 'pass_track_quality'
                #                            'Background' : 'background',
                #                            'TightLLH' : 'TightLLH'
                #                           }.items():
                #
                #         ## Get dataframe
                #         selection = 'opposite-sign_selection'
                #         # object = f'background-background-tag_background' if selection == 'background_selection' else f'{htype}-{htype}-tag_{htype}'
                #         object = f'{htype}-{htype}-tag_{htype}'
                #
                #         uid = f'{dtype}_{selection}_{object}_{x_label}-{y_label}-{z_label}'
                #         df = dfs[label][uid]
                #
                #         # Turn data into points
                #         value = 'entries'
                #         c = pandas_to_points(df, value, z_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))
                #
                #         ## Normalise MC curves for correct cross-section/luminosity
                #         if dtype == 'MC':
                #             # print('normalising', label, legend)
                #             mc_rundir = args.mc_rundir[0] if 'new' in label else rundirs[label]
                #             c = normalise_MC(c, label, mc_rundir, verbose=False)
                #
                #         curves[legend] = c
                #
                #         # print(legend, label, c[0][:3], c[1][:3])
                #         ymax = max(c[1]) if max(c[1]) > ymax else ymax
                #
                #     options = { 'fig_title' : f'{label} {dtype} {htype}',
                #                 'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                #                 'xlabel' : f'${latex[z_label]}$',
                #                 'ylabel' : 'Entries',
                #                 # 'logy' : True,
                #                 'logy' : False,
                #                 'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
                #                 'markers' : ['o', 'o', 'o', 'o'],
                #                 'linestyles' : ['-', '-', '-', '-'],
                #                 # 'ylim' : (0, 1.5*ymax*10),
                #                 # 'ylim' : (0, 1.5*ymax),
                #                 'xlim' : (60, 150),
                #                 }
                #     options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
                #     plot_from_points(curves,
                #                     output_name=f'{output_dir}/mll_{label}_{dtype}_{x_bin_str}_{y_bin_str}.png',
                #                     options=options)

            #     ## PLOT: data and MC together from 3D histograms
            #     model = 'TightLLH'
            #     obs_label = 'pt-primary_cluster_be2_eta-m'
            #     # for htype, dtype in it.product(['denominator', 'TightLLH'], ['Data', 'MC']):
            #     #     curves = {}
            #     #     ymax = 0
            #     #     for legend, selection in {
            #     #                               'Base'       : 'base_selection',
            #     #                               'Background' : 'background_selection'
            #     #                               }.items():
            #     for name in ['pass_track_quality', 'TightLLH']:
            #         print(f'> Plotting {name} distributions for bins {x_bin_str} and {y_bin_str}')
            #         curves = {}
            #         ymax = 0
            #         for legend, info in { f'{name} (Data)' : (name, 'Data'),
            #                               'Background (Data)' : ('background','Data'),
            #                               f'{name} (MC)' : (name, 'MC'),
            #                               }.items():
            #
            #             htype, dtype = info
            #
            #             ## Get dataframe
            #             selection = 'opposite-sign_selection'
            #             # object = f'background-background-tag_background' if selection == 'background_selection' else f'{htype}-{htype}-tag_{htype}'
            #             object = f'{htype}-{htype}-tag_{htype}'
            #
            #             uid = f'{dtype}_{selection}_{object}_{x_label}-{y_label}-{z_label}'
            #             df = dfs[label][uid]
            #
            #             # Turn data into points
            #             value = 'entries'
            #             c = pandas_to_points(df, value, z_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))
            #
            #             ## Normalise MC curves for correct cross-section/luminosity
            #             if dtype == 'MC':
            #                 # print('normalising', label, legend)
            #                 mc_rundir = args.mc_rundir[0] if 'new' in label else rundirs[label]
            #                 c = normalise_MC(c, label, mc_rundir, verbose=False)
            #
            #             curves[legend] = c
            #
            #             # print(legend, label, c[0][:3], c[1][:3])
            #             ymax = max(c[1]) if max(c[1]) > ymax else ymax
            #
            #         options = { 'fig_title' : f'{label} {name}',
            #                     'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
            #                     'xlabel' : f'${latex[z_label]}$',
            #                     'ylabel' : 'Entries',
            #                     'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
            #                     'markers' : ['o', 'o', 'o', 'o'],
            #                     'linestyles' : ['-', '-', '-', '-'],
            #                     'logy' : False,
            #                     'ylim' : (0, 1.5*ymax),
            #                     # 'xlim' : (60, 150),
            #                     }
            #         options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
            #         plot_from_points(curves,
            #                         output_name=f'{output_dir}/mll_{label}_{name}_{x_bin_str}_{y_bin_str}.png',
            #                         options=options)
            #
            # ## Create a montage with the projections (using imagemagick)
            # for name in ['denominator', 'TightLLH']:
            #     shared_name = f'mll_{label}_{name}'
            #     bash_command = "montage"
            #     for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_2.37_2.47']:
            #         for x_bin_str in [f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_80_150']:
            #             bash_command += f' {output_dir}/{shared_name}_{x_bin_str}_{y_bin_str}.png'
            #     montage_output = f'{output_dir}/combined_{shared_name}.png'
            #     bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
            #     process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
            #     out, error = process.communicate()
            #     print(f'> Saved {montage_output}')
            #
            # for dtype in ['Data', 'MC']:
            #     shared_name = f'mll_{label}_{dtype}'
            #     bash_command = "montage"
            #     for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_2.37_2.47']:
            #         for x_bin_str in [f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_80_150']:
            #             bash_command += f' {output_dir}/{shared_name}_{x_bin_str}_{y_bin_str}.png'
            #     montage_output = f'{output_dir}/combined_{shared_name}.png'
            #     bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
            #     process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
            #     out, error = process.communicate()
            #     print(f'> Saved {montage_output}')
            #
            if 'old' in label:
                continue
            # continue


            # ## PLOT: different selections 1D histograms for each htype, dtype
            # name = 'OSvsSS'
            # model = 'TightLLH'
            # for htype, dtype, obs_label in it.product(['pass_track_quality', 'TightLLH'], ['Data', 'MC'], ['m', 'charge']):
            #     curves = {}
            #     ymax = 0
            #     for legend, selection in {'No cut' : 'no-sign_selection',
            #                               # 'OS'     : 'base_selection',
            #                               'OS'     : 'opposite-sign_selection',
            #                               # 'SS'     : 'same-sign_selection',
            #                               'SS (veto)'  : 'same-sign-veto_selection'
            #                               }.items():

            #         ## Get dataframe
            #         # selection =
            #         object = f'tag_{htype}'

            #         uid = f'{dtype}_{selection}_{object}_{obs_label}'
            #         df = dfs[label][uid]

            #         # Turn data into points
            #         value = 'entries'
            #         c = pandas_to_points(df, value, obs_label)
            #         ## Using heppy:
            #         # x, y = df.curve()
            #         # bin_sizes = df.binwidths
            #         # yerr = np.zeros(len(y))
            #         # c = (x, y, bin_sizes, yerr)

            #         ## Normalise MC curves for correct cross-section/luminosity
            #         if dtype == 'MC':
            #             # print('normalising', label, legend)
            #             mc_rundir = args.mc_rundir[0] if 'new' in label else rundirs[label]
            #             c = normalise_MC(c, label, mc_rundir, verbose=False)

            #         curves[legend] = c

            #         # print(legend, label, c[0][:3], c[1][:3])
            #         ymax = max(c[1]) if max(c[1]) > ymax else ymax

            #     options = { 'fig_title' : f'{label} {dtype} {htype}',
            #                 'text' : f'{model}\n',#${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
            #                 'xlabel' : f'${latex[obs_label]}$',
            #                 'ylabel' : 'Entries',
            #                 'logy' : True if obs_label == 'm' else False,
            #                 'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
            #                 'markers' : ['o', 'o', 'o', 'o'],
            #                 'linestyles' : ['-', '-', '-', '-'],
            #                 'ylim' : (0, 1.5*ymax*10) if obs_label == 'm' else (0,1.5*ymax),
            #                 # 'xlim' : (60, 150),
            #                 # 'is_histogram' : True, # When using heppy
            #                 }
            #     options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
            #     # plot_from_points(curves,
            #     #                  output_name=f'{output_dir}/mll_{label}_{name}_{dtype}_{htype}_{obs_label}.png',
            #     #                  options=options)

            #     # No cut vs OS + SS
            #     ratio_curves = {}
            #     ratio_curves['No cut'] = curves['No cut']
            #     x, y, bin_sizes, yerr  = curves['OS']
            #     # r = y + curves['SS'][1]
            #     # ratio_curves['OS + SS'] = (x, r, bin_sizes, yerr)
            #     r = y + curves['SS (veto)'][1]
            #     ratio_curves['OS + SS (veto)'] = (x, r, bin_sizes, yerr)

            #     options = { 'fig_title' : f'{label} {dtype} {htype}',
            #                 'text' : f'{model}\n',#${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
            #                 'xlabel' : f'${latex[obs_label]}$',
            #                 'ylabel' : 'Entries',
            #                 'logy' : True if obs_label == 'm' else False,
            #                 'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
            #                 'markers' : ['o', 'o', 'o', 'o'],
            #                 'linestyles' : ['-', '-', '-', '-'],
            #                 'ylim' : (0, 1.5*ymax*10) if obs_label == 'm' else (0,1.5*ymax),
            #                 'ratio_ylabel' : 'Ratio'
            #                 # 'xlim' : (60, 150),
            #                 }
            #     options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
            #     plot_ratio_from_points(ratio_curves,
            #                     output_name=f'{output_dir}/mll_{label}_{name}_{dtype}_{htype}_{obs_label}_ratio.png',
            #                     options=options)

            #     print('AAAAAAA')
            #     print(ratio_curves['No cut'][1])
            #     print(ratio_curves['OS + SS (veto)'][1])
            ## PLOT: different objcts 1D histograms for each selection, dtype
            # model = 'TightLLH'
            # selection = 'opposite_selection_extra'
            # name = 'FailID'
            # for dtype, obs_label in it.product(['Data', 'MC'], ['m']):#, 'charge']):
            #     curves = {}
            #     ymax = 0
            #     for legend, htype in {'Denominator'       : 'denominator',
            #                           'VeryLooseLLH'      : 'VeryLooseLLH',
            #                           'FailVeryLooseLLH'  : 'FailVeryLooseLLH'
            #                           }.items():
            #
            #         ## Get dataframe
            #         # selection =
            #         object = f'tag_{htype}'
            #
            #         uid = f'{dtype}_{selection}_{object}_{obs_label}'
            #         df = dfs[label][uid]
            #
            #         # Turn data into points
            #         value = 'entries'
            #         # c = pandas_to_points(df, value, obs_label)
            #         x, y = df.curve()
            #         bin_sizes = df.binwidths
            #         yerr = np.zeros(len(y))
            #         c = (x, y, bin_sizes, yerr)
            #
            #         ## Normalise MC curves for correct cross-section/luminosity
            #         if dtype == 'MC':
            #             # print('normalising', label, legend)
            #             mc_rundir = args.mc_rundir[0] if 'new' in label else rundirs[label]
            #             c = normalise_MC(c, label, mc_rundir, verbose=False)
            #
            #         curves[legend] = c
            #
            #         # print(legend, label, c[0][:3], c[1][:3])
            #         ymax = max(c[1]) if max(c[1]) > ymax else ymax
            #
            #     options = { 'fig_title' : f'{label} {dtype}',
            #                 'text' : f'{model}\n',#${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
            #                 'xlabel' : f'${latex[obs_label]}$',
            #                 'ylabel' : 'Entries',
            #                 'logy' : True if obs_label == 'm' else False,
            #                 'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
            #                 'markers' : ['o', 'o', 'o', 'o'],
            #                 'linestyles' : ['-', '-', '-', '-'],
            #                 'ylim' : (0, 1.5*ymax*10),
            #                 'is_histogram' : True
            #                 # 'xlim' : (60, 150),
            #                 }
            #     options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
            #     plot_from_points(curves,
            #                     output_name=f'{output_dir}/mll_{label}_{name}_{dtype}_{obs_label}.png',
            #                     options=options)
            #
            #     # # Denominator vs VeryLooseLLH + FailVeryLooseLLH
            #     # ratio_curves = {}
            #     # ratio_curves['Denominator'] = curves['Denominator']
            #     # x, y, bin_sizes, yerr  = curves['VeryLooseLLH']
            #     # r = y + curves['FailVeryLooseLLH'][1]
            #     # ratio_curves['VeryLooseLLH + FailVeryLooseLLH'] = (x, r, bin_sizes, yerr)
            #     #
            #     # options = { 'fig_title' : f'{label} {dtype}',
            #     #             'text' : f'{model}\n',#${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
            #     #             'xlabel' : f'${latex[obs_label]}$',
            #     #             'ylabel' : 'Entries',
            #     #             'logy' : True if obs_label == 'm' else False,
            #     #             'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
            #     #             'markers' : ['o', 'o', 'o', 'o'],
            #     #             'linestyles' : ['-', '-', '-', '-'],
            #     #             'ylim' : (0, 1.5*ymax*10),
            #     #             'ratio_ylabel' : 'Ratio'
            #     #             # 'xlim' : (60, 150),
            #     #             }
            #     # options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
            #     # plot_ratio_from_points(ratio_curves,
            #     #                 output_name=f'{output_dir}/mll_{label}_{name}_{dtype}_{obs_label}_ratio.png',
            #     #                 options=options)
    ## Plot invariant mass distributions comparison
    if args.mll and (args.compare or args.only_compare):
        output_dir = f'{output}/comparison'
        makedirs(output_dir, exist_ok=True)

        ## PLOT: comparison of 3D for different selections

        ## Get x, y observables and their binnings
        df_ref = dfs[args.labels[0]]['Data_opposite-sign_selection_denominator-denominator-tag_denominator_pt-primary_cluster_be2_eta-m']
        obs_labels = [c.replace('bin_', '').replace('_lo', '')
                        for c in df_ref.columns if 'bin' in c and '_lo' in c]
        obs_binning = [np.union1d(df_ref['bin_' + obs + '_lo'].values,
                                df_ref['bin_' + obs + '_hi'].values)
                        for obs in obs_labels
                        ]
        observables = {x: (l, b) for x, l, b in zip(['x', 'y', 'z'], obs_labels, obs_binning)}
        x_label, x_binning = observables['x']
        y_label, y_binning = observables['y']
        z_label, z_binning = observables['z']

        ## Create 1D plots for fixed x bin and y bin
        for x_bin, y_bin in it.product(range(1, len(x_binning)), range(1, len(y_binning))):

            x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
            x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

            y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
            y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

            if x_bin_str not in [f'{x_label}_15_250', f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_80_150']: continue
            if y_bin_str not in [f'{y_label}_-2.47_2.47', f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue

            model = 'TightLLH'
            obs_label = 'pt-primary_cluster_be2_eta-m'
            for htype, dtype in it.product(['denominator', 'pass_track_quality', 'TightLLH', 'background'], ['Data', 'MC']):
                for selection_name, selection in {'Opposite-sign'       : 'opposite-sign_selection',
                                                  # 'Background' : 'background_selection',
                                                  'Same-sign' : 'same-sign_selection'
                                                  }.items():

                    if selection == 'same-sign_selection' and htype == 'background': continue

                    curves = {}
                    ymax = 0
                    for label in args.labels:

                        ## Get dataframe
                        # selection =
                        # object = f'background-background-tag_background' if selection == 'background_selection' else f'{htype}-{htype}-tag_{htype}'
                        object = f'{htype}-{htype}-tag_{htype}'

                        uid = f'{dtype}_{selection}_{object}_{x_label}-{y_label}-{z_label}'
                        df = dfs[label][uid]

                        df = df[ (df['bin_m_lo'] >= 60) & (df['bin_m_hi'] <= 250) ]

                        # Turn data into points
                        value = 'entries'
                        c = pandas_to_points(df, value, z_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                        ## Normalise MC curves for correct cross-section/luminosity
                        if dtype == 'MC':
                            # print('normalising', label, legend)
                            mc_rundir = args.mc_rundir[0] if 'new' in label else rundirs[label]
                            c = normalise_MC(c, label, mc_rundir, verbose=False)

                        curves[label] = c

                        ymax = max(c[1]) if max(c[1]) > ymax else ymax

                    options = { 'fig_title' : f'{selection_name} {dtype} {htype}',
                                'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                                'xlabel' : f'${latex[z_label]}$',
                                'ylabel' : 'Entries',
                                'logy' : False,
                                'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
                                'markers' : ['o', 'o', 'o', 'o'],
                                'linestyles' : ['-', '-', '-', '-'],
                                'ylim' : (0, 1.5*ymax),
                                # 'xlim' : (60, 150),
                                'ratio_ylim' : (0.9, 1.1)
                                }
                    options['xlabel'] = options['xlabel'] + ' [GeV]' if options['xlabel'] in ['m', 'pt'] else options['xlabel']
                    plot_ratio_from_points(curves,
                                           output_name=f'{output_dir}/mll_ratio_{dtype}_{selection_name}_{htype}_{x_bin_str}_{y_bin_str}.png',
                                           options=options)

        ## Create a montage with the projections (using imagemagick)
        for htype, dtype in it.product(['denominator', 'pass_track_quality' , 'TightLLH', 'background'], ['Data', 'MC']):
            for selection_name, selection in {'Opposite-sign'       : 'opposite-sign_selection',
                                                # 'Background' : 'background_selection',
                                                'Same-sign' : 'same-sign_selection'
                                                }.items():

                if selection == 'same-sign_selection' and htype == 'background': continue

                shared_name = f'mll_ratio_{dtype}_{selection_name}_{htype}'
                bash_command = "montage"
                for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_2.37_2.47']:
                    for x_bin_str in [f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_80_150']:
                        bash_command += f' {output_dir}/{shared_name}_{x_bin_str}_{y_bin_str}.png'
                montage_output = f'{output_dir}/combined_{shared_name}.png'
                bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
                process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
                out, error = process.communicate()
                print(f'> Saved {montage_output}')


    ## Plot efficiency for all models
    if args.eff and not args.only_compare:
        for label in args.labels:
            output_dir = f'{output}/{label}' if len(args.labels) > 1 else output
            makedirs(output_dir, exist_ok=True)
            for model, df in dfs[label].items():

                ## Plot 2D efficiency
                output_path = f'{output_dir}/eff_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff', label='Efficiency',
                                    title=f'Efficiency: {model.replace("_", " ")}')

                ## Plot 2D total error
                output_path = f'{output_dir}/error_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff_errup', label='Efficiency total error',
                                    title=f'Efficiency total error: {model.replace("_", " ")}')

                ## Plot 2D stat error
                output_path = f'{output_dir}/stats_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff_statup', label='Efficiency statistical error',
                                    title=f'Efficiency statistical error: {model.replace("_", " ")}')

                ## Plot 2D syst error
                output_path = f'{output_dir}/systs_{model}.png'
                plot_2D_from_pandas(df, output=output_path,
                                    values='eff_systup', label='Efficiency systematics error',
                                    title=f'Efficiency systematics error: {model.replace("_", " ")}')

                ## Plot 1D projections
                if args.projections:
                    projections_dir = f'{output_dir}/projections'
                    makedirs(projections_dir, exist_ok=True)

                    plot_1D_from_pandas({model : df}, output_dir=projections_dir,
                                        values='eff', label='Efficiency')

    ## Plot ratio for all models
    if args.eff and (args.compare or args.only_compare):
       output_dir = f'{output}/comparison' if args.projections else output
       makedirs(output_dir, exist_ok=True)

       shared_models = [i for i in dfs[args.labels[0]] if i in dfs[args.labels[1]]]
       print(f'Shared models to be compared: {shared_models}')

       for model in shared_models:

           d = {label : dfs[label][model] for label in args.labels}
           label1, df1 = list(d.items())[0]
           label2, df2 = list(d.items())[1]
           # print(df1['bin_pt_lo'].to_numpy()[0], df1['bin_pt_hi'].to_numpy()[-1])
           # print(df2['bin_pt_lo'].to_numpy()[0], df2['bin_pt_hi'].to_numpy()[-1])
           if df1.shape[0] != df2.shape[0]:
               raise Exception(f'Dataframes do not have the same binning: {df1.shape} vs {df2.shape}\n{np.unique(df1["bin_pt_lo"].to_numpy())}\n{np.unique(df2["bin_pt_lo"].to_numpy())}')

           df_ratio = deepcopy(df1)
           df_ratio['eff'] = (df1['eff']/df2['eff'] - np.ones(df1['eff'].shape[0]))*100
           df_ratio['eff_errup'] = (df1['eff_errup']/df2['eff_errup'] - np.ones(df1['eff_errup'].shape[0]))*100


           ## Plot 2D ratio
           output_path = f'{output_dir}/ratio_{model}.png'
           plot_2D_from_pandas(df_ratio, output=output_path,
                               values='eff', label='Efficiency relative error (%)',
                               title=f'Efficiency relative error ({label1}/{label2} - 1)*100: {model}')

           output_path = f'{output_dir}/ratio_{model}_error.png'
           plot_2D_from_pandas(df_ratio, output=output_path,
                               values='eff_errup', label='Efficiency total error relative error (%)',
                               title=f'Efficiency total error relative error ({label1}/{label2} - 1)*100: {model}')

           ## Plot 1D ratio projections
           if args.projections:
               projections_dir = f'{output_dir}/projections'
               makedirs(projections_dir, exist_ok=True)

               plot_1D_from_pandas(d, output_dir=projections_dir,
                                   values='eff', label='Efficiency',
                                   name=model,
                                   ratio=args.compare)
if __name__ == '__main__':
    main()
