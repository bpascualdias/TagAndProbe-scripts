## Version 1 (2022-03-10)
## This script plots the invariant mass distirbutions from the
## output of the tp efficiency command.
## Command:
##    python plot_mll.py -r <rundir> -l <label> -o <output_directory>

from os import makedirs, listdir, readlink
import argparse
import ROOT
import pandas as pd
import numpy as np
import seaborn as sns
import itertools as it
from copy import deepcopy
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

class LaTeX(dict):
    __missing__ = lambda self, key: key
latex = LaTeX({ 'pt' : 'p_{{T}}',
                'eta' : '\eta',
                'primary_cluster_be2_eta' : '\eta',
                'm' : 'm_{{ee}}'
               })

## Returns the number of events in the sample (from CutBookkeeper_XYZXYZ_1_NOSYS)
def get_N_sample(rundir):
    root_file = f'{rundir}/{readlink(f"{rundir}/histograms.root")}'
    tfile = ROOT.TFile.Open(root_file ,"READ")
    hist = [key.GetName() for key in tfile.GetListOfKeys() if 'CutBookkeeper' in key.GetName()][0]
    h = tfile.Get(hist)
    if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
    h.SetDirectory(0)
    tfile.Close()

    N_histogram = h.GetBinContent(1) # take contents of first bin
    N_sample = h.GetBinContent(2) # take contents of second bin

    return N_sample

## Load ROOT histogram and transform it to a pandas dataframe
def root_to_pandas(hist, root_file=None, values='values', x='x', y='y', z='z'):

    # Get histogram
    if root_file is None:
        h = hist
    else:
        tfile = ROOT.TFile.Open(root_file ,"READ")
        h = tfile.Get(hist)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist, root_file))
        h.SetDirectory(0)
        tfile.Close()

    # Get histogram data
    data = []
    if isinstance(h, ROOT.TH2):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin) / 1000
            x_up = h.GetXaxis().GetBinUpEdge(x_bin) / 1000
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            eff = h.GetBinContent(x_bin, y_bin)
            err = h.GetBinError(x_bin, y_bin)
            data.append((eff, err, x_dn, x_up, y_dn, y_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi']

    if isinstance(h, ROOT.TH3):
        x_binedges = np.array(h.GetXaxis().GetXbins())
        y_binedges = np.array(h.GetYaxis().GetXbins())
        z_binedges = np.array(h.GetZaxis().GetXbins())

        for x_bin, y_bin in it.product(range(1, h.GetNbinsX()+1), range(1, h.GetNbinsY()+1)): # index of a ROOT histogram axis, so must start at 1

            x_dn = h.GetXaxis().GetBinLowEdge(x_bin)
            x_up = h.GetXaxis().GetBinUpEdge(x_bin)
            y_dn = h.GetYaxis().GetBinLowEdge(y_bin)
            y_up = h.GetYaxis().GetBinUpEdge(y_bin)

            if x_dn > 1000.0:
                x_dn = x_dn/1000.0
                x_up = x_up/1000.0

            for z_bin in range(1, h.GetNbinsZ()+1):

                z_dn = h.GetZaxis().GetBinLowEdge(z_bin)
                z_up = h.GetZaxis().GetBinUpEdge(z_bin)

                mll = h.GetBinContent(x_bin, y_bin, z_bin)
                mll_err = h.GetBinError(x_bin, y_bin, z_bin)
                data.append((mll, mll_err, x_dn, x_up, y_dn, y_up, z_dn, z_up))

        columns = [values, f'{values}_err', f'bin_{x}_lo', f'bin_{x}_hi', f'bin_{y}_lo', f'bin_{y}_hi', f'bin_{z}_lo', f'bin_{z}_hi']

    # Create pandas dataframe with the histogram data
    data = np.array(data)
    df = pd.DataFrame(data, columns=columns)
    return df

## Get points from pandas:
def pandas_to_points(df, value, obs_label,
                     x_label='', x_bin=(None,None),
                     y_label='', y_bin=(None,None),
                     binning=None):

    entries = [c for c in df.columns if 'bin' not in c]
    x_bin_dn, x_bin_up = x_bin
    y_bin_dn, y_bin_up = y_bin
    x_bin_exists = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)].shape[0]
    y_bin_exists = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)].shape[0]
    if not x_bin_exists:
        p = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]
        p = df.groupby([f'bin_{obs_label}_lo', f'bin_{obs_label}_hi'], as_index=False)[entries].agg('sum')
    elif not y_bin_exists:
        p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)]
        p = df.groupby([f'bin_{obs_label}_lo', f'bin_{obs_label}_hi'], as_index=False)[entries].agg('sum')
    else:
        p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up) &
            (df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]

    y = p[value].to_numpy()
    #yerr = np.array([p[f'{value}_errdown'].abs().to_numpy(), p[f'{value}_eff_errup'].to_numpy()])
    #yerr = p[f'{value}_err'].to_numpy()
    yerr = np.zeros(len(y))
    bin_edges = np.unique(p[f'bin_{obs_label}_lo'].to_list() + p[f'bin_{obs_label}_hi'].to_list())
    bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
    x = p[f'bin_{obs_label}_lo'].to_numpy() + bin_sizes/2
    c = (x, y, bin_sizes, yerr)

    if binning is not None:
        print(binning)

    return c

## Plot from data points
def plot_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, figsize=(8, 8))#,
                           #gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})

    ## Plot curve points
    for i, label in enumerate(curves.keys()):
        x, y, w, yerr = curves[label]
        color = options['colors'][i] if 'colors' in options else 'black'
        marker = options['markers'][i] if 'markers' in options else 'o'
        linestyle = options['linestyles'][i] if 'linestyles' in options else 'none'
        ax.errorbar(x, y, xerr=w/2, yerr=yerr, label=label, marker=marker, linestyle=linestyle, color=color)

    ## Create legend box for the colors
    ax.legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ax.annotate(options['text'], xy=(0.05, 0.85), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax.semilogy()
    if 'ylim' in options.keys():
        ax.set_ylim(options['ylim'])
    if 'xlim' in options.keys():
        ax.set_xlim(options['xlim'])
    #ax.set_ylim(top=1e10)
    if 'fig_title' in options:
        ax.set_title(options['fig_title'])
    ax.set_xlabel(options['xlabel'], loc='right')
    ax.set_ylabel(options['ylabel'], loc='top')
    ax.yaxis.grid(True, alpha=0.5, which='major')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax.tick_params(which='major', axis='both', length=12)
    ax.tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

## Normalise MC curves
def normalise_MC(c, label, rundir='', verbose=False):

    lumi_weight = 59937.2 #58450.1 # pb^{-1} from https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data18_13TeV/20190219/notes.txt
    cross_section = 1901.1*1*1 # pb (cross-section * filter efficiency * k-factor) from https://ami.in2p3.fr/
    if 'new' in label:
        N_sample = get_N_sample(rundir) # number of events in the sample (from CutBookkeeper_XYZXYZ_1_NOSYS)
    if 'old' in label:
        N_sample =  512995200 # raw number of events in the sample (from ami)

    unweighted_mc_entries = c[1]
    scale_value = (1/N_sample) * cross_section * lumi_weight
    if verbose:
        print(f'Scale value for {label} = {scale_value}')
    weighted_mc_entries = unweighted_mc_entries * scale_value

    updated_mc = list(c)
    updated_mc[1] = weighted_mc_entries
    new_c = tuple(updated_mc)

    return new_c

## Plot ratio from data points
def plot_ratio_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 8),
                           gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})
    ## Plot first curve points
    label1 = list(curves.keys())[0]
    x1, y1, w1, yerr1 = curves[label1]
    ax[0].errorbar(x1, y1, xerr=w1/2, yerr=yerr1, label=label1,
                   marker=options['markers'][0] if 'markers' in options else 'o',
                   linestyle=options['linestyles'][0] if 'linestyles' in options else 'none',
                   color=options['colors'][0] if 'colors' in options else 'black')

    ## Plot second curve points
    label2 = list(curves.keys())[1]
    x2, y2, w2, yerr2 = curves[label2]
    ax[0].errorbar(x2, y2, xerr=w2/2, yerr=yerr2, label=label2,
                   marker=options['markers'][1] if 'markers' in options else 'o',
                   linestyle=options['linestyles'][1] if 'linestyles' in options else 'none',
                   color=options['colors'][1] if 'colors' in options else 'tab:red')

    ## Create legend box for the colors
    ax[0].legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ypos = 0.85 if len(options['text'].split('\n')) < 2 else 0.8
        ax[0].annotate(options['text'], xy=(0.05, ypos), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax[0].semilogy()
    if 'ylim' in options:
        ax[0].set_ylim(options['ylim'])
    if 'fig_title' in options:
        ax[0].set_title(options['fig_title'])
    ax[0].set_ylabel(options['ylabel'], loc='top')
    ax[0].yaxis.grid(True, alpha=0.5, which='major')
    ax[0].xaxis.set_minor_locator(AutoMinorLocator())
    ax[0].yaxis.set_minor_locator(AutoMinorLocator())
    ax[0].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[0].tick_params(which='major', axis='both', length=12)
    ax[0].tick_params(which='minor', axis='both', length=8)

    ## Plot ratio
    r = y1/y2
    ax[1].errorbar(x2, r, xerr=w2/2, marker='o', linestyle='none', color='black')
    #ax[1].hlines(1.0, min(x2), max(x2), color='black')
    if 'ratio_ylim' in options:
        ax[1].set_ylim(options['ratio_ylim'])
    ax[1].set_ylabel(f'{label1}/{label2}', loc='center')
    ax[1].set_xlabel(options['xlabel'], loc='right')
    ax[1].yaxis.grid(True, alpha=0.5, which='both')
    ax[1].yaxis.set_minor_locator(AutoMinorLocator())
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[1].tick_params(which='major', axis='both', length=12)
    ax[1].tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

## Load histograms from tp-analyse
def tp_analyse_to_pandas(root_file, model, mev_to_gev, pt_lim):
    tfile = ROOT.TFile.Open(root_file ,"READ")

    ## Load invariant mass histograms
    df_hists = {}
    hists_translator = {
        'N_num' : model, 'N_den' : 'denominator',     # base selection (selection)
        'BT_den' : 'background',                        # background template (selection)
        'N_ss_num' : model,                        # same-sign template (selection)
    }
    for values, htype in hists_translator.items():
        if 'BT' in values: tp = 'background_selection'
        elif 'ss' in htype: tp = 'same-sign_selection'
        else: tp = 'base_selection'

        ## Invariant mass histograms
        x_label = 'pt'
        y_label = 'primary_cluster_be2_eta'
        z_label = 'm'
        obs = f'{x_label}-{y_label}-{z_label}'
        hist = f'{htype}-{htype}-tag_{htype}'

        hist_name = f'{tp}/nominal/{hist}/reco/{obs}'
        h = tfile.Get(hist_name)
        if not h: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
        h.SetDirectory(0)

        ## Transform ROOT histogram into pandas dataframe
        ## NOTE: make sure to adapt the observables accordingly
        df = root_to_pandas(h, values=values, x=x_label, y=y_label, z=z_label)

        df_hists[values] = df

    ## Merge with other dataframes
    shared_columns = [c for c in df_hists['N_num'].columns if 'bin' in c]
    df = pd.merge(df_hists['N_num'], df_hists['N_den'], on=shared_columns)
    df = pd.merge(df, df_hists['BT_den'], on=shared_columns)
    df = pd.merge(df, df_hists['N_ss_num'], on=shared_columns)

    ## Convert MeV to GeV, if applicable
    if mev_to_gev:
        df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
        df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

    ## Define pt limits
    if pt_lim[0] != None:
        df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
    if pt_lim[1] != None:
        df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

    ## Hard-coded mass limits
    df = df[ (df['bin_m_lo'] >= 60) & (df['bin_m_hi'] <= 250) ]

    tfile.Close()
    return df

## Load histograms from TagAndProbeFrame
def old_to_pandas(root_file, model, dtype, mev_to_gev, pt_lim):
    tfile = ROOT.TFile.Open(root_file ,"READ")
    # dirs = [key.GetName() for key in tfile.GetListOfKeys() if model in key.GetName()]

    ## Load invariant mass histograms
    df_hists = {}
    hists_translator = {
        'N_num' : 'Num', 'N_den' : 'Den',             # base selection (selection)
        'BT_num' : 'NumTempl', 'BT_den' : 'DenTempl', # background template (selection)
        'N_ss_num' : 'Num_SS',                        # same-sign template (selection)
    }
    for values, htype in hists_translator.items():
        # ## master
        # models_full_name = lambda m : f'{m}_d0z0_DataDriven_Rel21_Smooth_vTest'
        # hist_name = ( f'{models_full_name(model)}'
        #             + f'_TemplRun2Variation1AndPassTrackQuality_TagTightLLH_DataDriven_Rel21_Smooth_vTest'
        #             + f'/{models_full_name(model)}'
        #             + f'_TemplRun2Variation1AndPassTrackQuality_TagTightLLH_DataDriven_Rel21_Smooth_vTest'
        #             )
        ## Zmass_fit_branch
        models_full_name = lambda m : f'{m}_d0z0_v13'
        hist_name = ( f'{models_full_name(model)}'
                    + f'_TemplFailVeryLooseLLHAndIso01_Cone30_TagTightLLH_d0z0_v13'
                    + f'/{models_full_name(model)}'
                    + f'_TemplFailVeryLooseLLHAndIso01_Cone30_TagTightLLH_d0z0_v13'
                    )
        if dtype == 'Data': hist_name += f'_Data_{htype}_Mll_3d'
        if dtype == 'MC': hist_name += f'_MCZee_NoFilter_{htype}_Mll_3d'

        h_mll = tfile.Get(hist_name)
        if not h_mll: print("Failed to get histogram\n hist_name = %s\n root_file = %s" % (hist_name, root_file))
        h_mll.SetDirectory(0)

        ## Transform ROOT histogram into pandas dataframe
        ## NOTE: make sure to adapt the observables accordingly
        x_label = 'pt'
        y_label = 'primary_cluster_be2_eta'
        z_label = 'm'
        df = root_to_pandas(h_mll, values=values, x=x_label, y=y_label, z=z_label)

        df_hists[values] = df

    ## Merge with other dataframes
    shared_columns = [c for c in df_hists['N_num'].columns if 'bin' in c]
    df = pd.merge(df_hists['N_num'], df_hists['N_den'], on=shared_columns)
    df = pd.merge(df, df_hists['BT_num'], on=shared_columns)
    df = pd.merge(df, df_hists['BT_den'], on=shared_columns)
    df = pd.merge(df, df_hists['N_ss_num'], on=shared_columns)

    ## Convert MeV to GeV, if applicable
    if mev_to_gev:
        df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
        df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

    ## Define pt limits
    if pt_lim[0] != None:
        df = df[df['bin_pt_lo'] >= pt_lim[0]].reset_index(drop=True)
    if pt_lim[1] != None:
        df = df[df['bin_pt_hi'] <= pt_lim[1]].reset_index(drop=True)

    tfile.Close()
    return df


def main():
    ## Define parser arguments and load them
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-o', '--output', help='Name of the output directory where the plots will be saved.', nargs=1, dest='output', default="output")
    parser.add_argument('-r', '--rundirs', help='List of run directories obtained as teh output of tp efficiency', nargs='+', dest='rundirs', default=[])
    parser.add_argument('-l', '--labels', help='Labels to identify the run directories. This will be used, for instance, in the legend of the plots.', nargs='+', dest='labels', default=[])
    parser.add_argument('-m', '--models', help='Name of the efficiency models to consider.', nargs='+', dest='models', default=[])
    parser.add_argument('-b', '--mc_rundir', help='Run directory of the mc analysis, containing the CutBookkeper histogram.', nargs=1, dest='mc_rundir', default='')
    parser.add_argument('-p', '--projections', help='If passed, plot the projections for certain bins.', dest='projections', action='store_true', default=False)
    parser.add_argument('-c', '--compare', help='If passed, plot the ratio of the two run directories passed.', dest='compare', action='store_true', default=False)
    parser.add_argument('-g', '--mev_to_gev', help='If passed, convert pt entries from MeV to GeV.', dest='mev_to_gev', action='store_true', default=False)
    parser.add_argument('--pt_lim', help='Set minimum and maximum pt in the form of a tuple.', nargs=1, dest='pt_lim', type=str, default=['None, None'])
    args = parser.parse_args()

    ## Create output directory
    output = args.output[0].replace('/', '') if len(args.output) else 'output'
    makedirs(output, exist_ok=True)

    ## Sanity checks in the arguments
    if args.compare and len(args.rundirs) != 2:
        raise Exception('Please provide only two run directories when performing a comparison.')
    if len(args.rundirs) != len(args.labels):
        raise Exception('Please provide a list of labels that matches the rundirs in order.')

    ## Create mapping of labels to rundirs
    rundirs = {label : rundir for label, rundir in zip(args.labels, args.rundirs)}

    ## Set models to run over
    models = args.models if len(args.models) != 0 else [i.replace('_central_value.pkl', '').split('_')[-1] for i in listdir(args.rundirs[0]) if 'central_value' in i]

    ## Evaluate arguments
    pt_lim = eval(args.pt_lim[0])

    ## Load histograms
    print('> Loading histograms...')
    dfs = {}
    for label, rundir in rundirs.items():
        dfs[label] = {}

        if 'tp-efficiency' in rundir:
            ## New framework efficiency output
            print('> Reading output from tp efficiency')
            for dtype, model in it.product(['Data', 'MC'], models):
                df = pd.read_pickle(f'{rundir}/histograms_{dtype}_{model}_mll_75_105.pkl')

                df = df[ (df['bin_m_lo'] >= 60) & (df['bin_m_hi'] <= 250) ]
                df = df[ (df['bin_pt_lo'] >= 15) ]

                dfs[label][f'{dtype}_{model}'] = df

                ## Convert MeV to GeV, if applicable
                if args.mev_to_gev:
                    df['bin_pt_lo'] = df['bin_pt_lo'].div(1000)
                    df['bin_pt_hi'] = df['bin_pt_hi'].div(1000)

                ## Store the dataframe in a dictionary
                uid = f'{dtype}_{model}'
                dfs[label][uid] = df

        elif len([f for f in listdir(rundir) if 'tp-selection' in f]) != 0:
            ## New framework selection output
            print('> Reading output from tp analyse')
            for dtype, model in it.product(['Data', 'MC'], models):

                if dtype == 'Data':
                    data_dir = [f for f in listdir(rundir) if 'tp-selection' in f and 'data' in f ][0]
                if dtype == 'MC':
                    data_dir = [f for f in listdir(rundir) if 'tp-selection' in f and 'data' not in f ][0]
                print(data_dir)
                root_file = f'{rundir}/{data_dir}/{readlink(f"{rundir}/{data_dir}/histograms.root")}'

                df = tp_analyse_to_pandas(root_file, model, args.mev_to_gev, pt_lim)

                uid = f'{dtype}_{model}'
                dfs[label][uid] = df

        else:
            ## Old framework selection output
            print('> Reading output from TagAndProbeFrame')
            for dtype, model in it.product(['Data', 'MC'], models):
                root_file = f'{rundir}/merged-hist-{dtype.lower()}.root'
                print(root_file)

                df = old_to_pandas(root_file, model, dtype, args.mev_to_gev, pt_lim)

                uid = f'{dtype}_{model}'
                dfs[label][uid] = df

    print('Histograms found:')
    s = ''
    for label in dfs.keys():
        s += f'  {label}: '
        for uid in dfs[label].keys():
            s += f'{uid} '
        s += '\n'
    print(s)

    ## Get x, y observables and their binnings
    df_ref = dfs[args.labels[0]][f'Data_TightLLH']
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                    for c in df_ref.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df_ref['bin_' + obs + '_lo'].values,
                            df_ref['bin_' + obs + '_hi'].values)
                    for obs in obs_labels
                    ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y', 'z'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']
    z_label, z_binning = observables['z']

    ## Create 1D plots for fixed x bin and y bin
    for x_bin, y_bin in it.product(range(1, len(x_binning)), range(1, len(y_binning))):

        integrate = ''
        if integrate == 'x':
            x_bin_dn, x_bin_up = x_binning[0], x_binning[-1]
            x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

            y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
            y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'
        elif integrate == 'y':
            x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
            x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

            y_bin_dn, y_bin_up = y_binning[0], y_binning[-1]
            y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'
        else:
            x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
            x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

            y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
            y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

        if x_bin_str not in [f'{x_label}_15_20', f'{x_label}_40_45', f'{x_label}_{x_binning[0]:.0f}_{x_binning[-1]:.0f}']: continue
        if y_bin_str not in [f'{y_label}_0.10_0.60', f'{y_label}_{y_binning[0]:.2f}_{y_binning[-1]:.2f}']: continue
        #if x_bin_str not in [f'{x_label}_15_20', f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']: continue
        #if y_bin_str not in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue

        if not args.compare:
            for label in args.labels:
                output_dir = f'{output}/{label}' if len(args.labels) > 1 else output
                makedirs(output_dir, exist_ok=True)
                # ## Plot DATA and MONTE-CARLO for numerator and denominator
                # ## NOTE: only works in the tp-efficiency output
                # for model, htype in it.product(models, ['den', 'num']):

                #     if 'tp-efficiency' not in rundirs[label]: continue

                #     curves = {}
                #     ymax = 0
                #     for dtype, value in [('Data','N_'+htype), ('Data','B_'+htype), ('MC','N_'+htype)]:

                #         uid = f'{dtype}_{model}'
                #         df = dfs[label][uid]
                #         obs_label = 'm'

                #         c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                #         if 'N_' in value: legend = f'Base {dtype}'
                #         if 'BT_' in value: legend = f'Background {dtype}'
                #         if 'B_' in value: legend = f'Bkg estimation'


                #         ## Normalise MC curves for correct cross-section/luminosity
                #         if dtype == 'MC':
                #             c = normalise_MC(c, label, args.mc_rundir[0])
                #         curves[legend] = c

                #         print(legend, c[0][:3], c[1][:3])
                #         ymax = max(c[1]) if max(c[1]) > ymax else ymax

                #     # Add curve with MC + bkg
                #     mc_bkg = list(curves['Base MC'])
                #     mc_bkg[1] = curves['Base MC'][1] + curves['Bkg estimation'][1]
                #     curves['Expected (MC + bkg)'] = tuple(mc_bkg)

                #     # If numerator, add same-sign distribution
                #     if htype == 'num':
                #         dtype, value = 'Data', 'N_ss_num'
                #         uid = f'{dtype}_{model}'
                #         df = dfs[label][uid]
                #         c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))
                #         curves['SS'] = c

                #     options = { 'fig_title' : f'{label} {htype}',
                #                 'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                #                 'xlabel' : f'${latex[obs_label]}$' + ' [GeV]',
                #                 'ylabel' : 'Entries / 1 GeV',
                #                 'logy' : False if htype == 'den' else True,
                #                 'colors' : ['black', 'tab:blue', 'tab:orange', 'tab:red', 'tab:green'],
                #                 'markers' : ['o', 'o', '', '', 'o'],
                #                 'linestyles' : ['', '', '-', '-', ''],
                #                 'ylim' : (0, 1.5*ymax) if htype == 'den' else (0, 1.5*10*ymax),
                #                 # 'xlim' : (60, 150),
                #                 }
                #     plot_from_points(curves,
                #                     output_name=f'{output}/mll_{label}_{model}_{htype}_{x_bin_str}_{y_bin_str}.png',
                #                     options=options)

                # ## RAW TEMPLATES: lot DATA and MONTE-CARLO for numerator and denominator
                # hists = { 'num': [('Data','N_num'), ('Data','N_ss_num'), ('MC','N_num')],
                #           'den': [('Data','N_den'), ('Data','BT_den'), ('MC','N_den'), ('MC','BT_den')],
                #          }
                # for model, htype in it.product(models, hists.keys()):

                #     curves = {}
                #     ymax = 0
                #     for dtype, value in hists[htype]:

                #         uid = f'{dtype}_{model}'
                #         df = dfs[label][uid]
                #         obs_label = 'm'
                #         c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                #         if 'BT_' in value: legend = f'Background'
                #         if 'N_' in value and not 'ss' in value: legend = f'Base'
                #         if 'N_ss' in value: legend = f'Same-sign'
                #         legend += f' {dtype}'

                #         ## Normalise MC curves for correct cross-section/luminosity
                #         if dtype == 'MC':
                #             c = normalise_MC(c, label, args.mc_rundir[0])
                #         curves[legend] = c

                #         print(legend, c[0][:3], c[1][:3])
                #         ymax = max(c[1]) if max(c[1]) > ymax else ymax

                #     # # Add curve with MC + bkg
                #     # mc_bkg = list(curves['Base MC'])
                #     # if htype == 'den':
                #     #     mc_bkg[1] = curves['Base MC'][1] + curves['Background Data'][1]
                #     # if htype == 'num':
                #     #     mc_bkg[1] = curves['Base MC'][1] + curves['Same-sign Data'][1]
                #     # curves['Expected (MC + bkg)'] = tuple(mc_bkg)

                #     options = { 'fig_title' : f'{label} {model} {htype}',
                #                 'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                #                 'xlabel' : f'${latex[obs_label]}$' + ' [GeV]',
                #                 'ylabel' : 'Entries / 1 GeV',
                #                 'logy' : True, #if htype == 'num' else False,
                #                 'colors' : ['black', 'tab:blue', 'tab:orange', 'tab:red', 'tab:green'],
                #                 'markers' : ['o', 'o', '', '', 'o'],
                #                 'linestyles' : ['-', '-', '-', '-', ''],
                #                 'ylim' : (0, 1.5*ymax),
                #                 # 'xlim' : (60, 150),
                #                 }
                #     plot_from_points(curves,
                #                     output_name=f'{output_dir}/mll_{label}_{model}_{htype}_{x_bin_str}_{y_bin_str}.png',
                #                     options=options)

                ## Plot selection histograms
                for model, dtype in it.product(models, ['Data','MC']):

                    curves = {}
                    ymax = 0
                    for value in ['N_den', 'BT_den', 'N_num', 'N_ss_num']:

                        uid = f'{dtype}_{model}'
                        df = dfs[label][uid]
                        obs_label = 'm'

                        c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                        if 'BT_' in value: legend = f'Background'
                        if 'N_' in value and not 'ss' in value: legend = f'Base'
                        if 'N_ss' in value: legend = f'Same-sign'
                        legend += f' {dtype} {value.split("_")[-1]}'

                        ## Normalise MC curves for correct cross-section/luminosity
                        if dtype == 'MC':
                            # print('normalising', label, legend)
                            mc_rundir = args.mc_rundir[0] if 'new' in label else ''
                            c = normalise_MC(c, label, mc_rundir, verbose=True)
                        curves[legend] = c

                        print(legend, label, c[0][:3], c[1][:3])
                        ymax = max(c[1]) if max(c[1]) > ymax else ymax

                    options = { 'fig_title' : f'{label} {model} {dtype}',
                                'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                                'xlabel' : f'${latex[obs_label]}$' + ' [GeV]',
                                'ylabel' : 'Entries / 1 GeV',
                                'logy' : False,
                                'colors' : ['black', 'tab:red', 'tab:blue', 'tab:green'],
                                'markers' : ['o', 'o', 's', 's'],
                                'linestyles' : ['-', '-', '-', '-'],
                                'ylim' : (0, 1.5*ymax),
                                # 'xlim' : (60, 150),
                                }
                    plot_from_points(curves,
                                    output_name=f'{output_dir}/mll_{label}_{model}_{dtype}_{x_bin_str}_{y_bin_str}.png',
                                    options=options)

        if args.compare:
            output_dir = f'{output}/comparison'
            makedirs(output_dir, exist_ok=True)
            ## Plot comparison of each histogram
            for model, dtype, value in it.product(models, ['Data', 'MC'], ['N_den', 'N_num', 'N_ss_num', 'BT_den']):

                curves = {}
                ymax = 0
                for label in args.labels:

                    uid = f'{dtype}_{model}'
                    df = dfs[label][uid]
                    obs_label = 'm'

                    c = pandas_to_points(df, value, obs_label, x_label=x_label, x_bin=(x_bin_dn, x_bin_up), y_label=y_label, y_bin=(y_bin_dn, y_bin_up))

                    if 'B_' in value: legend = f'Bkg estimation'
                    if 'BT_' in value: legend = f'Background'
                    if 'N_' in value and not 'ss' in value: legend = f'Base'
                    if 'N_ss' in value: legend = f'Same-sign'
                    legend += f' {dtype} {value.split("_")[-1]}'

                    ## Normalise MC curves for correct cross-section/luminosity
                    if dtype == 'MC':
                        # print('normalising', label, legend)
                        c = normalise_MC(c, label, args.mc_rundir[0], verbose=False)
                    curves[label] = c

                    print(legend, label, c[0][:3], c[1][:3])
                    ymax = max(c[1]) if max(c[1]) > ymax else ymax

                options = { 'fig_title' : f'{legend}',
                            'text' : f'{model}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$ [GeV]\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                            'xlabel' : f'${latex[obs_label]}$' + ' [GeV]',
                            'ylabel' : 'Entries / 1 GeV',
                            'logy' : False,
                            'colors' : ['black', 'tab:red'],
                            'markers' : ['o', 'o'],
                            'linestyles' : ['-', '-'],
                            'ylim' : (0, 1.5*ymax),
                            # 'xlim' : (60, 150),
                            }
                plot_ratio_from_points(curves,
                                output_name=f'{output_dir}/mll_ratio_{model}_{dtype}_{value}_{x_bin_str}_{y_bin_str}.png',
                                options=options)


if __name__ == '__main__':
    main()
