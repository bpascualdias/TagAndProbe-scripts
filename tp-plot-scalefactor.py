## This script plots scale factors from the efficiency results in given run directory.
## Command: python plot_scalefactors.py -r <run_directory> -m <model1> <model2> -o <plots_directory>

from os import makedirs, listdir
import argparse
import pandas as pd
import numpy as np
import seaborn as sns
from copy import deepcopy
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

#plt.rcParams['font.sans-serif'] = 'Helvetica'

class LaTeX(dict):
    __missing__ = lambda self, key: key
latex = LaTeX({ 'pt' : 'p_{{T}}',
                'eta' : '\eta',
                'primary_cluster_be2_eta' : '\eta',
               })

def plot_2D_from_pandas(df, title, output, values='eff', label='Efficiency'):

    ## Get observables and their binnings
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df['bin_' + obs + '_lo'].values,
                              df['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    x_unit = ' [GeV]' if x_label == 'pt' else ''
    y_unit = ' [GeV]' if y_label == 'pt' else ''

    ## Set matplotlib global parameters
    plt.rc('font', size=10)

    ## Create a 2D plot from the pandas dataframe
    fig, ax = plt.subplots(figsize=(8, 6), tight_layout=True)
    heatmap_data = pd.pivot_table(df, values=values, index=[f'bin_{y_label}_lo'], columns=f'bin_{x_label}_lo')
    xticklabels = [f'{i:.0f}'.format(i) for i in heatmap_data.columns.to_numpy()]
    yticklabels = [f'{i:.2f}'.format(i) for i in heatmap_data.index.to_numpy()]
    ax = sns.heatmap(heatmap_data, annot=True, cmap='Spectral',
                     xticklabels=xticklabels, yticklabels=yticklabels,
                     vmax=df[values].max(), fmt='.2',
                     cbar_kws={'label': label})
    ax.invert_yaxis()
    plt.xticks(np.arange(len(x_binning)), [f'{i:.0f}'.format(i) for i in x_binning], rotation='horizontal')
    plt.yticks(np.arange(len(y_binning)), [f'{i:.2f}'.format(i) for i in y_binning], rotation='horizontal')
    ax.set_xlabel(f'${latex[x_label]}${x_unit}')
    ax.set_ylabel(f'${latex[y_label]}${y_unit}')
    ax.set_title(title)

    ## Save the plot
    fig.savefig(output, dpi=300)
    print(f'> Saved {output}')
    plt.close(fig)

def plot_1D_ratio_from_pandas(df1, df1_label, df2, df2_label, output_dir,
                              label, title, ratio_ylim=(0.8, 1.0)):

    ## Get observables and their binnings
    obs_labels = [c.replace('bin_', '').replace('_lo', '')
                  for c in df1.columns if 'bin' in c and '_lo' in c]
    obs_binning = [np.union1d(df1['bin_' + obs + '_lo'].values,
                              df1['bin_' + obs + '_hi'].values)
                   for obs in obs_labels
                  ]
    observables = {x: (l, b) for x, l, b in zip(['x', 'y'], obs_labels, obs_binning)}
    x_label, x_binning = observables['x']
    y_label, y_binning = observables['y']

    ## Create 1D plots for fixed x bin
    for x_bin in range(1, len(x_binning)):

        x_bin_dn, x_bin_up = x_binning[x_bin-1], x_binning[x_bin]
        x_bin_str = f'{x_label}_{x_bin_dn:.0f}_{x_bin_up:.0f}'

        if x_bin_str not in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']: continue

        curves = []
        for df in [df1, df2]:
            p = df[(df[f'bin_{x_label}_lo'] == x_bin_dn) & (df[f'bin_{x_label}_hi'] == x_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            #yerr = p['eff_errup'].to_numpy()
            bin_edges = np.unique(p[f'bin_{y_label}_lo'].to_list() + p[f'bin_{y_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{y_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves.append(c)

        options = { 'fig_title' : title,
                    'label' : [df1_label, df2_label],
                    'text' : f'{label}\n${x_bin_dn:.0f} < {latex[x_label]} < {x_bin_up:.0f}$',
                    'xlabel' : f'${latex[y_label]}$',
                    'ylabel' : 'Efficiency',
                    'ylim' : (0.55, 1.05),
                    'ratio_ylim' : (0.8, 1.1)
                  }
        options['text'] = options['text'] + ' [GeV]' if x_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if y_label == 'pt' else options['xlabel']
        plot_ratio_from_points(curves,
                               output_name=f'{output_dir}/scale_factors_{label}_{x_bin_str}.png',
                               options=options)

    ## Create 1D plots for fixed y bin
    for y_bin in range(1, len(y_binning)):

        y_bin_dn, y_bin_up = y_binning[y_bin-1], y_binning[y_bin]
        y_bin_str = f'{y_label}_{y_bin_dn:.2f}_{y_bin_up:.2f}'

        if y_bin_str not in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']: continue

        curves = []
        for df in [df1, df2]:
            p = df[(df[f'bin_{y_label}_lo'] == y_bin_dn) & (df[f'bin_{y_label}_hi'] == y_bin_up)]
            y = p['eff'].to_numpy()
            yerr = np.array([p['eff_errdown'].abs().to_numpy(), p['eff_errup'].to_numpy()])
            bin_edges = np.unique(p[f'bin_{x_label}_lo'].to_list() + p[f'bin_{x_label}_hi'].to_list())
            bin_sizes = np.array([bin_edges[i+1]-bin_edges[i] for i in range(0, len(bin_edges)-1)])
            x = p[f'bin_{x_label}_lo'].to_numpy() + bin_sizes/2
            c = (x, y, bin_sizes, yerr)

            curves.append(c)

        options = { 'fig_title' : title,
                    'label' : [df1_label, df2_label],
                    'text' : f'{label}\n${y_bin_dn:.2f} < {latex[y_label]} < {y_bin_up:.2f}$',
                    'xlabel' : f'${latex[x_label]}$',
                    'ylabel' : 'Efficiency',
                    'ylim' : (0.55, 1.05),
                    'ratio_ylim' : (0.8, 1.1)
                  }
        options['text'] = options['text'] + ' [GeV]' if y_label == 'pt' else options['text']
        options['xlabel'] = options['xlabel'] + ' [GeV]' if x_label == 'pt' else options['xlabel']
        plot_ratio_from_points(curves,
                               output_name=f'{output_dir}/scale_factors_{label}_{y_bin_str}.png',
                               options=options)

    ## Create a montage with the projections (using imagemagick)
    bash_command = "montage"
    for x_bin_str in [f'{x_label}_25_30', f'{x_label}_50_60', f'{x_label}_80_150']:
        bash_command += f' {output_dir}/scale_factors_{label}_{x_bin_str}.png'
    for y_bin_str in [f'{y_label}_0.10_0.60', f'{y_label}_1.52_1.81', f'{y_label}_2.37_2.47']:
        bash_command += f' {output_dir}/scale_factors_{label}_{y_bin_str}.png'
    montage_output = f'{output_dir}/combined_scale_factors_{label}.png'
    bash_command += f' -tile 3x3 -geometry +0+0 -density 300 {montage_output}'
    process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
    out, error = process.communicate()
    print(f'> Saved {montage_output}')


def plot_ratio_from_points(curves, output_name, options):

    ## Set matplotlib global parameters
    plt.rc('font', size=18)

    ## Start figure
    fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 8),
                           gridspec_kw={'height_ratios': [3, 1], 'hspace': 0})
    ## Plot first curve points
    x1, y1, w1, yerr1 = curves[0]
    ax[0].errorbar(x1, y1, xerr=w1/2, yerr=yerr1, label=options['label'][0], marker='o', linestyle='none', color='black')

    ## Plot second curve points
    x2, y2, w2, yerr2 = curves[1]
    ax[0].errorbar(x2, y2, xerr=w2/2, yerr=yerr2, label=options['label'][1], marker='o', linestyle='none', color='tab:red')

    ## Create legend box for the colors
    ax[0].legend(loc='upper right')

    ## Create text outside the legend
    if 'text' in options:
        ax[0].annotate(options['text'], xy=(0.05, 0.85), xycoords='axes fraction', ha='left')

    ## Finish the plot
    if 'logy' in options and options['logy']:
        ax[0].semilogy()
    ylim = options['ylim'] if 'ylim' in options.keys() else (0.6, 1.1)
    ax[0].set_ylim(ylim)
    ax[0].set_ylabel(options['ylabel'], loc='top')
    ax[0].yaxis.grid(True, alpha=0.5, which='major')
    ax[0].xaxis.set_minor_locator(AutoMinorLocator())
    ax[0].yaxis.set_minor_locator(AutoMinorLocator())
    ax[0].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[0].tick_params(which='major', axis='both', length=12)
    ax[0].tick_params(which='minor', axis='both', length=8)

    ## Plot ratio
    r = y1/y2
    ax[1].errorbar(x2, r, xerr=w2/2, marker='o', linestyle='none', color='black')
    #ax[1].hlines(1.0, min(x2), max(x2), color='black')
    ratio_ylim = options['ratio_ylim'] if 'ratio_ylim' in options.keys() else (0.8, 1.0)
    ax[1].set_ylim(ratio_ylim)
    ax[1].set_ylabel(f'{options["label"][0]}/{options["label"][1]}', loc='center')
    ax[1].set_xlabel(options['xlabel'], loc='right')
    ax[1].yaxis.grid(True, alpha=0.5, which='both')
    ax[1].yaxis.set_minor_locator(AutoMinorLocator())
    ax[1].xaxis.set_minor_locator(AutoMinorLocator())
    ax[1].tick_params(which='both', axis='both', direction='in', top=True, right=True)
    ax[1].tick_params(which='major', axis='both', length=12)
    ax[1].tick_params(which='minor', axis='both', length=8)

    ## Save the plot
    fig.savefig(f'{output_name}', dpi=300, bbox_inches='tight')
    print(f'> Saved {output_name}')
    plt.close(fig)

def main():
    ## Define parser arguments and load them
    parser = argparse.ArgumentParser(description='', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-o', '--output', help='', nargs=1, dest='output', default="")
    parser.add_argument('-r', '--rundir', help='', nargs=1, dest='rundir', default="")
    parser.add_argument('-m', '--models', help='', nargs='+', dest='models', default="")
    parser.add_argument('-p', '--projections', help='', dest='projections', action='store_true', default=False)
    args = parser.parse_args()

    rundir = args.rundir[0]
    models = args.models
    output = args.output[0].replace('/', '') if len(args.output) else 'output'
    makedirs(output, exist_ok=True)

    print(f'> Getting efficiencies from {rundir}')
    ## Plot scale-factors for all models
    for model in models:

        df_data = pd.read_pickle(f'{rundir}/efficiency_Data_{model}_central_value.pkl')
        df_mc = pd.read_pickle(f'{rundir}/efficiency_MC_{model}_central_value.pkl')

        df_sf = deepcopy(df_data)
        df_sf['sf'] = df_data['eff']/df_mc['eff']

        output_path = f'{output}/scale_factors_{model}.png'
        plot_2D_from_pandas(df_sf, output=output_path,
                            values='sf', label='Scale Factors',
                            title=f'Scale Factors: {model}')

        if args.projections:
            plot_1D_ratio_from_pandas(df_data, 'Data', df_mc, 'MC',
                                      output_dir=output,
                                      label=model,
                                      title=f'Scale Factors: {model}')

if __name__ == '__main__':
    main()
